# LaBEL elit archive

Interface prototype for e-literature archive. Querying a SparQL-endpoint with in-memory cache for performance.  

![](research/setup.svg)

## Interface prototype at `prototype/label`

The interface prototype uses the [DAAP](https://daap.network/) SparQL-endpoint.

### Install

To install the prototype clone this repository and install its dependencies.

- cd into `prototype/label`
- Install with `pip install -r requirements.txt`


### Running

- cd into `prototype/label`
- start the interface by running the bash script: `bash run.sh`

The interface should be accessible at `http://localhost:5000/`


# Django prototype

making a venv

    python3 -m venv ~/venv/label
    source ~/venv/label/bin/activate

## To install:


- Install requirements `pip install -r prototype/django/requirements.txt`
- Run migrations: `prototype/django/label/manage.py migrate`
- Create a superuser: `prototype/django/label/manage.py createsuperuser`
- Put the test data in: `prototype/django/label/label/management/commands/`

## Loading test data:

- Fetch test data: `prototype/django/label/manage.py fetch_test_data`

## Starting the server

- Run server: `prototype/django/label/manage.py fetch_test_data`
- The interface should be accessible at <http://localhost:8000>