from flask import Flask, abort, render_template, redirect, request
from .query import cached_query
from .paginator import Paginator

app = Flask(__name__)


def getEntityId (url):
  prefix = 'https://daap.bannerrepeater.org/entity/Q'
  if url.startswith(prefix):
    return int(url[len(prefix):])
  else:
    return None


def getPropertyId (url):
  prefix = 'https://daap.bannerrepeater.org/entity/P'
  if url.startswith(prefix):
    return int(url[len(prefix):])
  else:
    return None


@app.template_filter('inFilters')
def inFilters(taxon, activeFilters):
  if taxon['prop'] in activeFilters \
    and taxon['value'] in activeFilters[taxon['prop']]:
    return "True"
  
  return False


@app.template_filter('getInternalUrl')
def getInternalUrl (url):
  parsers = [
    (getEntityId, '/entity/{}'),
    (getPropertyId, '/taxonomy/{}')
  ]
  
  for (parser, replacement) in parsers:
    identifier = parser(url)
    if identifier is not None:
      return replacement.format(identifier)
    
  return None

@app.template_filter('extractItemID')
def extractItemID (url):
  return getEntityId(url)

@app.template_filter('fileOriginalURL')
def originalImgURL (url):
  return url.replace("wiki/File:","wiki/Special:Redirect/file/")


@app.route("/taxonomy/<int:id>")
def taxonomy (id=None):
  if not id:
    abort(400)
  else:
    q =  f'''SELECT DISTINCT ?prop ?wbtype ?propLabel ?propDescription ?value ?valueLabel ?valueDescription WHERE {{
      BIND (wd:P{id} AS ?prop) .
      ?prop wikibase:propertyType ?wbtype.
      # Used to extract the label
      ?subject wdt:P{id} ?value .
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}'''

    taxa = cached_query(q)

    return render_template('taxonomy.html', taxa=taxa)
  


@app.route("/entity/<int:id>")
def entity (id=None):
  if not id:
    abort(400)
  else:
    q = f'''SELECT ?prop ?propLabel ?propDescription ?wbtype ?value ?valueLabel ?valueDescription ?source ?sourceLabel ?sourceDescription WHERE {{
      BIND (wd:Q{id} AS ?source)
      ?source ?p ?value.
      ?prop wikibase:directClaim ?p .
      ?prop wikibase:propertyType ?wbtype.
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}'''

    claims = cached_query(q)

    if not claims:
      q = f'''SELECT ?source ?sourceLabel ?sourceDescription WHERE {{
          BIND (wd:Q{id} AS ?source)
          SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
        }}
      '''

      claims = cached_query(q)

    return render_template('entity.html', claims=claims, id=id)


@app.route("/reverse/<int:id>")
def reverse (id=None):
  if not id:
    abort(400)
  else:
    q = f'''SELECT ?prop ?propLabel ?propDescription ?wbtype ?value ?valueLabel ?valueDescription ?source ?sourceLabel ?sourceDescription WHERE {{
      BIND (wd:Q{id} AS ?source)
      ?value ?p ?source.
      ?prop wikibase:directClaim ?p .
      ?prop wikibase:propertyType ?wbtype.
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}'''

    claims = cached_query(q)

    return render_template('reverse.html', claims=claims, id=id)


@app.route("/works")
@app.route("/works/<int:page>")
def works (page=1):
  q = '''SELECT ?work ?workLabel ?image ?date WHERE {
      ?work wdt:P1 wd:Q1;
      # Entities with 'instance of' (P1) Work (Q1) statement
        wdt:P13 ?date.
        # Find attached dates
      FILTER(?work != wd:Q57)
      # Filter out 'Example work page' (Q57)
      
      OPTIONAL {
        ?work p:P90 [ ps:P90 ?image; pq:P54 wd:Q90 ].
        # work has statement 'image' (P90) 
        # with qualifier 'depicts' (P54) 'front cover' (Q90)
      }

      SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }      
    }
    GROUP BY ?work ?workLabel ?image ?date
    ORDER BY ?workLabel'''

  works = cached_query(q)

  return render_template('works.html', paginator=Paginator(works, page=page, url='/works/{}'))



@app.route('/search', methods=["GET", "POST"])
def search ():
  props = {
    'format': 16,
    'material': 23,
  }

  q =  f'''SELECT DISTINCT ?prop ?wbtype ?propLabel ?propDescription ?value ?valueLabel ?valueDescription WHERE {{
    BIND (wd:P{ props['format'] } AS ?prop) .
    ?prop wikibase:propertyType ?wbtype.
    # Used to extract the label
    ?subject wdt:P{ props['format'] } ?value .
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
  }}'''

  taxa = cached_query(q)

  results = []

  if request.method == 'POST':
    # request.form is ImmutableMultidict 
    # https://werkzeug.palletsprojects.com/en/3.0.x/datastructures/#werkzeug.datastructures.ImmutableMultiDict
    # With property urls as keys and list of entity ids as values
    # Treat both keys and values to make them easier to read and use them in queries
    
    queryFilterParts = []

    for propUrl, entityUrls in request.form.lists():
      prop = getPropertyId(propUrl)
      entities = [f'wd:Q{ getEntityId(url) }' for url in entityUrls ]
      entityString = ', '.join(entities)
      queryFilterParts.append(f'?result wdt:P{ prop } ?{ prop }. FILTER(?{ prop } IN({ entityString }))')

    if queryFilterParts:
      print(queryFilterParts)
      queryFilterString = '\n'.join(queryFilterParts)
      print(queryFilterString)

      q = f'''
        SELECT ?result ?resultLabel ?resultDescription WHERE {{
          { queryFilterString }
          SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
        }}
      '''

      print(q)

      results = cached_query(q)

  return render_template('search.html', taxa=taxa, results=results, activeFilters=dict(request.form.lists()))

@app.route('/')
def index ():
  return redirect('/works')