from SPARQLWrapper import SPARQLWrapper2, JSON
import logging, sys
from functools import cache

sparql = SPARQLWrapper2("https://query.daap.bannerrepeater.org/proxy/wdqs/bigdata/namespace/wdq/sparql")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

logging.basicConfig(filename='myapp.log', level=logging.DEBUG)
sparql.setReturnFormat(JSON)

# Todo auto date parsing

# Parse result
# Each row is transformed into an dictionary with variableName as key,
# value is the value in the result if the binding exists, else None
def parseBindings (bindings, keys):
  # Todo recognize more complex data types
  # Deal with identifier prefix

  for binding in bindings:
    yield {
      k: binding[k].value if k in binding else None for k in keys
    }


def query (q, keys=None):
  logger.debug(f'Setting up sparql request: {q}.')
  sparql.setQuery(q)
  logger.debug('Sending request, parsing results.')
  r = sparql.query()
  logger.debug('Request complete.')
  logger.debug('Parsing results.')
  return parseBindings(r.bindings, r.variables)


@cache
def cached_query (q):
  return list(query(q))

if __name__ == '__main__':
  q = '''SELECT ?work ?workLabel ?image ?date WHERE {
    ?work wdt:P1 wd:Q1;
    # Entities with 'instance of' (P1) Work (Q1) statement
      wdt:P13 ?date.
      # Find attached dates
    FILTER(?work != wd:Q57)
    # Filter out 'Example work page' (Q57)
    
    OPTIONAL {
      ?work p:P90 [ ps:P90 ?image; pq:P54 wd:Q90 ].
      # work has statement 'image' (P90) 
      # with qualifier 'depicts' (P54) 'front cover' (Q90)
    }

    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }      
  }
  GROUP BY ?work ?workLabel ?image ?date
  ORDER BY ?workLabel'''


  try:
    # Loop through rows
    for row in query(q):
      print(row)
      # print(binding['work'].value, binding['workLabel'].value)
      # print(binding)

  except Exception as e:
    print(e)