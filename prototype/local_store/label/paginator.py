import math

class Paginator:
  def __init__ (self, session, stmt, count_stmt, page=1, url='/{}', page_size=10):
    self.row_count = session.execute(count_stmt).scalar_one()
    self.page_size = page_size
    self.url = url
    self.page_count = math.ceil(self.row_count / page_size)
    self.page = max(1, min(page, self.page_count))
    self.data = list(session.scalars(stmt.offset((self.page - 1) * self.page_size).limit(self.page_size)).unique())

  def go_to (self, page):
    if page < self.page_count:
      self.page = self.page

  @property
  def rows (self):
    return self.data
  
  @property
  def previous_page (self):
    if self.page > 1:
      return self.page - 1
    else:
      return None
  
  @property
  def previous_url (self):
    previous_page = self.previous_page

    if previous_page:
      return self.url.format(previous_page)
    else:
      return None
  
  @property
  def next_page (self):
    if self.page < self.page_count:
      return self.page + 1
    else:
      return None
  
  @property
  def next_url (self):
    next_page = self.next_page

    if next_page:
      return self.url.format(next_page)
    else:
      return None