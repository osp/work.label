
let details = document.querySelectorAll('details');
for(let detail of details){
    detail.addEventListener('click', function(){
        let iframe = detail.querySelector('iframe');
        iframe.src = iframe.dataset.src;
        detail.parentNode.scrollTop = 0;
    });
}