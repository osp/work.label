from typing import List
from sqlalchemy import ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship
from label.db import db
from flask import g

def translation (Field):
    return relationship(
        foreign_keys=[Field],
        lazy='joined',
        primaryjoin=lambda: (Field==TextInLanguage.term_id) & (TextInLanguage.language_id==g.current_language)
    )

class Language (db.Model):
    __tablename__ = 'language'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    code: Mapped[str]
    name: Mapped[str]


"""
  id: Q91:label
      Q91:description

      P54:label
      P54:description

      ItemClaim:{id}:text
"""
"""
  TODO: add index on term_id and term_id.language_id
"""
class TextInLanguage (db.Model):
    __tablename__ = 'text_in_language'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    term_id: Mapped[str]
    language_id: Mapped[int] = mapped_column(ForeignKey('language.id'))
    text: Mapped[str]

   
# class ItemClaimQualifier (db.Model):
#     __tablename__ = 'item_claim_qualifier'

#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     order: Mapped[int]
#     subject_id: Mapped[int] = mapped_column(ForeignKey('item_claim.id'))
#     property_id: Mapped[int] = mapped_column(ForeignKey('property.id'))
#     object_id: Mapped[int] = mapped_column(ForeignKey('item.id'), nullable=True)
#     text: Mapped[str] = mapped_column(nullable=True)
#     type: Mapped[str] = mapped_column(String(25), nullable=True)

#     subject: Mapped["ItemClaim"] = relationship(back_populates="qualifiers", foreign_keys=[subject_id])
#     property: Mapped["Property"] = relationship(lazy='joined', back_populates="in_item_claim_qualifiers")
#     object: Mapped["Item"] = relationship(lazy='joined', foreign_keys=[object_id])


# class ItemClaim (db.Model):
#     __tablename__ = 'item_claim'

#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     subject_id: Mapped[int] = mapped_column(ForeignKey('item.id'))
#     property_id: Mapped[int] = mapped_column(ForeignKey('property.id'))
#     object_id: Mapped[int] = mapped_column(ForeignKey('item.id'), nullable=True)
#     text: Mapped[str] = mapped_column(nullable=True)
#     type: Mapped[str] = mapped_column(String(25), nullable=True)

#     subject: Mapped["Item"] = relationship(back_populates="claims", foreign_keys=[subject_id])
#     property: Mapped["Property"] = relationship(lazy='joined', back_populates="in_item_claims")
#     object: Mapped["Item"] = relationship(lazy='joined', foreign_keys=[object_id])
#     qualifiers: Mapped[List["ItemClaimQualifier"]] = relationship(back_populates="subject", foreign_keys=[ItemClaimQualifier.subject_id])


# class Item (db.Model):
#     __tablename__ = 'item'

#     id: Mapped[int] = mapped_column(primary_key=True)
#     wb_id: Mapped[str]
#     shallow: Mapped[bool] # Shallow entities have a label and description, but no properties

#     label_id: Mapped[str] = mapped_column(ForeignKey('text_in_language.term_id'), nullable=True)
#     description_id: Mapped[str] = mapped_column(ForeignKey('text_in_language.term_id'), nullable=True)
#     # wikidata_id: Mapped[int]
#     # label: Mapped[str] = mapped_column(nullable=True)
#     # description: Mapped[str] = mapped_column(nullable=True)

#     label: Mapped["TextInLanguage"] = translation(label_id)
#     description: Mapped["TextInLanguage"] = translation(description_id)
    
    
    
#     claims: Mapped[List["ItemClaim"]] = relationship(back_populates="subject", foreign_keys=[ItemClaim.subject_id])


# class PropertyClaimQualifier (db.Model):
#     __tablename__ = 'property_claim_qualifier'

#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     subject_id: Mapped[int] = mapped_column(ForeignKey('property_claim.id'))
#     property_id: Mapped[int] = mapped_column(ForeignKey('property.id'))
#     object_id: Mapped[int] = mapped_column(ForeignKey('item.id'), nullable=True)
#     text: Mapped[str] = mapped_column(nullable=True)
#     type: Mapped[str] = mapped_column(String(25), nullable=True)
#     order: Mapped[int]
    
#     subject: Mapped["PropertyClaim"] = relationship(back_populates="qualifiers", foreign_keys=[subject_id])
#     property: Mapped["Property"] = relationship(lazy='joined', back_populates="in_property_claim_qualifiers")
#     object: Mapped["Item"] = relationship(lazy='joined', foreign_keys=[object_id])


# class PropertyClaim (db.Model):
#     __tablename__ = 'property_claim'

#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     subject_id: Mapped[int] = mapped_column(ForeignKey('property.id'))
#     property_id: Mapped[int] = mapped_column(ForeignKey('property.id'))
#     object_id: Mapped[int] = mapped_column(ForeignKey('item.id'), nullable=True)
#     text: Mapped[str] = mapped_column(nullable=True)
#     type: Mapped[str] = mapped_column(String(25), nullable=True)

#     subject: Mapped["Property"] = relationship(back_populates="claims", foreign_keys=[subject_id])
#     property: Mapped["Property"] = relationship(lazy='joined', back_populates="in_property_claims", foreign_keys=[property_id])
#     object: Mapped["Item"] = relationship(lazy='joined', foreign_keys=[object_id])
#     qualifiers: Mapped[List["PropertyClaimQualifier"]] = relationship(back_populates="subject", foreign_keys=[PropertyClaimQualifier.subject_id])



# class Property (db.Model):
#     __tablename__ = 'property'

#     id: Mapped[int] = mapped_column(primary_key=True)
#     wb_id: Mapped[str]
#     shallow: Mapped[bool] # Shallow entities have a label and description, but no properties

#     label_id: Mapped[str] = mapped_column(ForeignKey('text_in_language.term_id'), nullable=True)
#     description_id: Mapped[str] = mapped_column(ForeignKey('text_in_language.term_id'), nullable=True)

#     label: Mapped["TextInLanguage"] = translation(label_id)
#     description: Mapped["TextInLanguage"] = translation(description_id)

#     claims: Mapped[List["PropertyClaim"]] = relationship(back_populates="subject", foreign_keys=[PropertyClaim.subject_id])

#     in_item_claims: Mapped[List["ItemClaim"]] = relationship(foreign_keys=ItemClaim.property_id)
#     in_item_claim_qualifiers: Mapped[List["ItemClaimQualifier"]] = relationship(foreign_keys=ItemClaimQualifier.property_id)

#     in_property_claims: Mapped[List["PropertyClaim"]] = relationship(foreign_keys=PropertyClaim.property_id)
#     in_property_claim_qualifiers: Mapped[List["PropertyClaimQualifier"]] = relationship(foreign_keys=PropertyClaimQualifier.property_id)
#     # claims: Mapped[List["Claim"]] = relationship(secondary=property_claim)

class EntityType (db.Model):
    __tablename__ = 'entity_type'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    type: Mapped[str]


class EntityClaimQualifier (db.Model):
    __tablename__ = 'entity_claim_qualifier'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    subject_id: Mapped[int] = mapped_column(ForeignKey('entity_claim.id'))
    property_id: Mapped[int] = mapped_column(ForeignKey('entity.id'))
    object_id: Mapped[int] = mapped_column(ForeignKey('entity.id'), nullable=True)
    text: Mapped[str] = mapped_column(nullable=True)
    type: Mapped[str] = mapped_column(String(25), nullable=True)
    order: Mapped[int]
    
    subject: Mapped["EntityClaim"] = relationship(back_populates="qualifiers", foreign_keys=[subject_id])
    property: Mapped["Entity"] = relationship(lazy='joined', back_populates="property_in_claim_qualifiers", foreign_keys=[property_id])
    object: Mapped["Entity"] = relationship(lazy='joined', foreign_keys=[object_id])


class EntityClaim (db.Model):
    __tablename__ = 'entity_claim'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    subject_id: Mapped[int] = mapped_column(ForeignKey('entity.id'))
    property_id: Mapped[int] = mapped_column(ForeignKey('entity.id'))
    object_id: Mapped[int] = mapped_column(ForeignKey('entity.id'), nullable=True)
    text: Mapped[str] = mapped_column(nullable=True)
    type: Mapped[str] = mapped_column(String(25), nullable=True)

    subject: Mapped["Entity"] = relationship(back_populates="claims", foreign_keys=[subject_id])
    property: Mapped["Entity"] = relationship(lazy='joined', back_populates="property_in_claims", foreign_keys=[property_id])
    object: Mapped["Entity"] = relationship(lazy='joined', foreign_keys=[object_id])
    qualifiers: Mapped[List["EntityClaimQualifier"]] = relationship(back_populates="subject", foreign_keys=[EntityClaimQualifier.subject_id])



class Entity (db.Model):
    __tablename__ = 'entity'

    # id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    id: Mapped[str] = mapped_column(primary_key=True)

    # identifier: Mapped[str] = mapped_column(primary_key=True)
    wb_identifier: Mapped[str]

    shallow: Mapped[bool] # Shallow entities have a label and description, but no properties
    type_id = mapped_column(ForeignKey('entity_type.id'), nullable=True)
    label_id: Mapped[str] = mapped_column(ForeignKey('text_in_language.term_id'), nullable=True)
    description_id: Mapped[str] = mapped_column(ForeignKey('text_in_language.term_id'), nullable=True)
    
    type: Mapped["EntityType"] = relationship(foreign_keys=[type_id])

    data_type: Mapped[str] = mapped_column(nullable=True)

    label: Mapped["TextInLanguage"] = translation(label_id)
    description: Mapped["TextInLanguage"] = translation(description_id)

    claims: Mapped[List["EntityClaim"]] = relationship(back_populates="subject", foreign_keys=[EntityClaim.subject_id])

    property_in_claims: Mapped[List["EntityClaim"]] = relationship(foreign_keys=EntityClaim.property_id)
    property_in_claim_qualifiers: Mapped[List["EntityClaimQualifier"]] = relationship(foreign_keys=EntityClaimQualifier.property_id) 
