import os.path

def make_abs_path(*parts):
  return os.path.join(os.path.dirname(os.path.abspath(__file__)), *parts)
