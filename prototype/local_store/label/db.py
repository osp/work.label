from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase

from flask_sqlalchemy import SQLAlchemy

class Base(DeclarativeBase):
  pass

db = SQLAlchemy(model_class=Base)


# dbpath = make_abs_path('label.db')
# engine = create_engine("sqlite+pysqlite:///{}".format(dbpath), echo=True)
# # db_session = scoped_session(sessionmaker(autocommit=False,
# #                                          autoflush=False,
# #                                          bind=engine))