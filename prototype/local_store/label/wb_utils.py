import logging
from wikibaseintegrator.wbi_config import config as wbi_config
from wikibaseintegrator import WikibaseIntegrator
from wikibaseintegrator.wbi_enums import WikibaseDatatype, WikibaseDatePrecision
from wikibaseintegrator.wbi_exceptions import MissingEntityException
from label.app import db
from label.models import Entity, EntityClaim, EntityClaimQualifier, EntityType, Language, TextInLanguage
from sqlalchemy import select

import time
import datetime

logging.basicConfig(level=logging.DEBUG)

wbi_config['MEDIAWIKI_API_URL'] = 'https://daap.bannerrepeater.org/api.php'
wbi_config['SPARQL_ENDPOINT_URL'] = 'https://query.daap.bannerrepeater.org/proxy/wdqs/bigdata/namespace/wdq/sparql'
wbi_config['WIKIBASE_URL'] = 'https://query.daap.bannerrepeater.org/'

LOCAL_MEDIA_PREFIX = '/'
COMMONS_MEDIA_PREFIX = '/'

WIKIBASE_ITEM_ID = 'wikibase-itemid'
WIKIBASE_ENTITY_ID = 'wikibase-entityid'

MAX_DEPTH = 3

# def format_date (date):
#     if date.precision == YEAR:
#         date.date.strftime('%Y')
#     elif date.precision == MONTH:
#         date.date.strftime('%m-%Y')
#     elif date.precision == DAY:
#         date.date.strftime('%d-%m-%Y')


def parseStringValue (value):
    # "Speculative futures cover.png"
    return value

def parseItemIdValue (value):
    # {"item-type": "item", "numeric-id": 68, "id": "Q68"}
    return value['id']


def parseMonoLingualTextValue (value):
    # {"text": "Dream Babes: Speculative Futures", "language": "en"}
    return value['text']


def parseQuantityValue (value):
    # Amount is either a number, or a number with a unit.
    # Units themselves are Wikibase items.
    # Create a list somewhere of units. Also for data-
    # {"amount": "+2000", "unit": "1"}, "type": "quantity"}
    # { "amount": "+130", "unit": "https://daap.bannerrepeater.org/entity/Q38" }
    if '.' in value['amount']:
        return float(value['amount'])
    else:
        return int(value['amount'])
        

def parseTimeValue (value):
    # Dates can be encoded with a different pattern than
    # their precision suggests.
    # @FIXME: construct which includes precision for the template.
    patterns = [
        '+%Y-%m-%dT00:00:00Z',
        '+%Y-%m-00T00:00:00Z',
        '+%Y-00-00T00:00:00Z'
    ]
    for pattern in patterns:
        try:
            return datetime.datetime.strptime(value['time'], pattern)
        except ValueError:
            continue

    # # {"time": "+2017-03-15T00:00:00Z", "timezone": 0, "before": 0, "after": 0, "precision": 11, "calendarmodel": "http://www.wikidata.org/entity/Q1985727"}
    # if value['precision'] == WikibaseDatePrecision.DAY.value:
    #     return datetime.datetime.strptime(value['time'], '+%Y-%m-%dT00:00:00Z')
    # if value['precision'] == WikibaseDatePrecision.MONTH.value:
    #     return datetime.datetime.strptime(value['time'], '+%Y-%m-00T00:00:00Z')
    # if value['precision'] == WikibaseDatePrecision.YEAR.value:
    #     return datetime.datetime.strptime(value['time'], '+%Y-00-00T00:00:00Z')


def parseValue(value):
    if value['type'] == WikibaseDatatype.STRING.value:
        # {"value": "Speculative futures cover.png", "type": "string"}
        return parseStringValue(value['value'])
    elif value['type'] == WIKIBASE_ENTITY_ID or value['type'] == WIKIBASE_ITEM_ID:
        # {"value": {"entity-type": "item", "numeric-id": 68, "id": "Q68"}, "type": "wikibase-entityid"}, "datatype": "wikibase-item"}
        return parseItemIdValue(value['value'])
    elif value['type'] == WikibaseDatatype.MONOLINGUALTEXT.value:
        # {"value": {"text": "Dream Babes: Speculative Futures", "language": "en"}, "type": "monolingualtext"}, "datatype": "monolingualtext"}
        return parseMonoLingualTextValue(value['value'])
    elif value['type'] == WikibaseDatatype.TIME.value:
        # {"value": {"time": "+2017-03-15T00:00:00Z", "timezone": 0, "before": 0, "after": 0, "precision": 11, "calendarmodel": "http://www.wikidata.org/entity/Q1985727"}, "type": "time"}
        return parseTimeValue(value['value'])


def parseDatatypeItem(value):
    return parseValue(value)



def parseDatatypeTime(value):
    return parseValue(value)


def parseDatatypeLocalMedia(value):
    # Download copy of the media file?
    # Wrap in construct?
    return LOCAL_MEDIA_PREFIX + parseValue(value)


def parseDatatypeCommonsMedia(value):
    # Wrap in construct?
    # Download copy of the media file?
    return COMMONS_MEDIA_PREFIX + parseValue(value)


def parseDatatypeString (value):
    # Wrap in construct?
    return parseValue(value)


def parseDatatypeUrl (value):
    # Wrap in construct?
    return parseValue(value)


def parseDatatypeExternalId(value):
    # Wrap in construct?
    return parseValue(value)


wbi = WikibaseIntegrator()


def makeTextInLanguage(language_id, term_id, text):
    entry = TextInLanguage(
        term_id=term_id,
        language_id=language_id,
        text=text
    )
    db.session.add(entry)
    db.session.flush()
    
    return entry


def makeEntityClaimQualifier (wb_claim_qualifier, entity_claim, order, depth=0):
    qualifier_prop = getProperty(wb_claim_qualifier.property_number, depth)
    
    if qualifier_prop:
        value = None
        text = None

        if wb_claim_qualifier.datatype == WikibaseDatatype.ITEM.value:
            value = getItem(parseDatatypeItem(wb_claim_qualifier.datavalue), depth)

            if value is None:
                return None
        elif wb_claim_qualifier.datatype == WikibaseDatatype.PROPERTY.value:
            value = getProperty(parseDatatypeItem(wb_claim_qualifier.datavalue), depth)

            if value is None:
                return None
        elif wb_claim_qualifier.datatype == WikibaseDatatype.TIME.value:
            text = parseDatatypeTime(wb_claim_qualifier.datavalue)
        else:
            text = parseValue(wb_claim_qualifier.datavalue)

        claim_qualifier = EntityClaimQualifier(
            order=order,
            subject=entity_claim,
            property=qualifier_prop,
            object=value,
            text=text,
            type=wb_claim_qualifier.datatype)

        db.session.add(claim_qualifier)

        entity_claim.qualifiers.append(claim_qualifier)
        
        return claim_qualifier
    else:
        return None

def makeEntityClaim (wb_claim, entity, depth=0):
    claim_prop = getProperty(wb_claim.mainsnak.property_number, depth)

    if claim_prop:
        value = None
        text = None

        if wb_claim.mainsnak.datatype == WikibaseDatatype.ITEM.value:
            value = getItem(parseDatatypeItem(wb_claim.mainsnak.datavalue), depth)

            if value is None:
                # Could not find entity.
                return None  
        elif wb_claim.mainsnak.datatype == WikibaseDatatype.PROPERTY.value:
            value = getProperty(parseDatatypeItem(wb_claim.mainsnak.datavalue), depth)
            
            if value is None:
                # Could not find entity.
                return None
        elif wb_claim.mainsnak.datatype == WikibaseDatatype.TIME.value:
            text = parseDatatypeTime(wb_claim.mainsnak.datavalue)
        else:
            text = parseValue(wb_claim.mainsnak.datavalue)

        claim = EntityClaim(
            subject=entity,
            property=claim_prop,
            object=value,
            text=text,
            type=wb_claim.mainsnak.datatype)

        db.session.add(claim)

        entity.claims.append(claim)

        for idx, wb_claim_qualifier in enumerate(wb_claim.qualifiers):
            makeEntityClaimQualifier(wb_claim_qualifier, claim, idx, depth)
        
        return claim
    else:
        return None

def makeItem (qid, depth=0):
    print("Making item ", qid)
    try:
        wb_item = wbi.item.get(qid)
        
        label_id = f'item:label:{qid}'
        has_label = False
        description_id = f'item:description:{qid}'
        has_description = False

        for language in db.session.scalars(select(Language)):
            label = wb_item.labels.get(language.code)
            if label:
                makeTextInLanguage(language.id, label_id, label.value)
                has_label = True

            description = wb_item.descriptions.get(language.code)
            if description:
                makeTextInLanguage(language.id, description_id, description.value)
                has_description = True

        item = Entity(
            id=qid,
            wb_identifier=qid,
            type_id=1,
            shallow=(depth >= MAX_DEPTH),
            label_id=f'item:label:{qid}' if has_label else None,
            description_id=f'item:description:{qid}' if has_description else None
        )

        db.session.add(item)

        if not item.shallow:
            for wb_claim in wb_item.claims:
                # Mainsnak holds the value of the claim
                makeEntityClaim(wb_claim, item, depth)
        
        return item
    except MissingEntityException:
        print(f"Entity not found on remote {qid}")
        return None


def makeProperty (pid, depth=0):
    print("Making property", pid)
    try:
        wb_prop = wbi.property.get(pid)

        label = wb_prop.labels.get('en')
        description = wb_prop.descriptions.get('en')

        label_id = f'property:label:{pid}'
        has_label = False
        description_id = f'property:description:{pid}'
        has_description = False

        for language in db.session.scalars(select(Language)):
            label = wb_prop.labels.get(language.code)
            if label:
                makeTextInLanguage(language.id, label_id, label.value)
                has_label = True

            description = wb_prop.descriptions.get(language.code)
            if description:
                makeTextInLanguage(language.id, description_id, description.value)
                has_description = True

        
        prop = Entity(
            id=pid,
            wb_identifier=pid,
            type_id=2,
            shallow=(depth >= MAX_DEPTH),
            label_id=label_id if has_label else None,
            description_id=description_id if has_description else None
        )

        db.session.add(prop)

        if not prop.shallow:
            for wb_claim in wb_prop.claims:
                # Mainsnak holds the value of the claim
                # print(claim.mainsnak, claim.mainsnak.datatype, WikibaseDatatype.ITEM)
                makeEntityClaim(wb_claim, prop, depth)

        return prop
    except MissingEntityException:
        print(f"Entity not found on remote: {pid}")
        return None

def getItem (qid, depth=-1):
    # item = db.session.scalar(select(Entity).where(Entity.identifier==qid))
    item = db.session.get(Entity, qid)

    if not item:
        time.sleep(.3)
        return makeItem(qid, depth + 1)
    else:
        return item
    

def getProperty (pid, depth=0):
    # Fetch it from the database.
    # If it does not it exist fetch it from the API
    # prop = db.session.scalar(select(Entity).where(Entity.identifier==pid))
    prop = db.session.get(Entity, pid)

    if not prop:
        time.sleep(.4)
        return makeProperty(pid, depth)
    else:
        return prop
    

def getEntity (identifier, depth):
    if identifier.startswith('P'):
        return getProperty(identifier, depth)
    else:
        return getItem(identifier, depth)


def makeEntityTypes ():
    db.session.add(EntityType(type='item'))
    db.session.add(EntityType(type='property'))


def makeLanguages ():
    db.session.add(Language(code='en', name='English'))
    db.session.add(Language(code='nl', name='Dutch'))
    db.session.add(Language(code='fr', name='French'))

# print(getItem('Q6187'))