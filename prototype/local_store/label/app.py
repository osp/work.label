import click
from flask import Flask, abort, g, render_template, redirect, request
from label.paginator import Paginator
from label.utils import make_abs_path
from label.models import Entity, EntityClaim, EntityClaimQualifier, Language, EntityType
from label.db import db
from label.wb_utils import getItem, makeLanguages, makeEntityTypes

from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import desc, func, select, text
from sqlalchemy.orm import selectinload, joinedload

import itertools

CURRENT_LANGUAGE = 1

app = Flask(__name__)
dbpath = make_abs_path('label.db')
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite+pysqlite:///{}".format(dbpath)
app.config["SQLALCHEMY_ECHO"] = False
# initialize the app with the extension
db.init_app(app)

with app.app_context():
    db.create_all()

def getItemId (url):
  prefix = 'https://daap.bannerrepeater.org/Item/Q'
  if url.startswith(prefix):
    return int(url[len(prefix):])
  else:
    return None


def getPropertyId (url):
  prefix = 'https://daap.bannerrepeater.org/Item/P'
  if url.startswith(prefix):
    return int(url[len(prefix):])
  else:
    return None


@app.template_filter('inFilters')
def inFilters(taxon, activeFilters):
  if taxon['prop'] in activeFilters \
    and taxon['value'] in activeFilters[taxon['prop']]:
    return "True"
  
  return False


@app.template_filter('getInternalUrl')
def getInternalUrl (url):
  parsers = [
    (getItemId, '/Item/{}'),
    (getPropertyId, '/taxonomy/{}')
  ]
  
  for (parser, replacement) in parsers:
    identifier = parser(url)
    if identifier is not None:
      return replacement.format(identifier)
    
  return None

@app.template_filter('extractItemID')
def extractItemID (url):
  return getItemId(url)

@app.template_filter('fileOriginalURL')
def originalImgURL (url):
  return url.replace("wiki/File:","wiki/Special:Redirect/file/")


# @app.route("/taxonomy/<int:id>")
# def taxonomy (id=None):
#   if not id:
#     abort(400)
#   else:
#     q =  f'''SELECT DISTINCT ?prop ?wbtype ?propLabel ?propDescription ?value ?valueLabel ?valueDescription WHERE {{
#       BIND (wd:P{id} AS ?prop) .
#       ?prop wikibase:propertyType ?wbtype.
#       # Used to extract the label
#       ?subject wdt:P{id} ?value .
#       SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
#     }}'''

#     taxa = cached_query(q)

#     return render_template('taxonomy.html', taxa=taxa)

"""
  List all the properties
"""
@app.route("/properties")
def properties():
  g.current_language = CURRENT_LANGUAGE

  stmt = (
      select(Entity)
        .where(Entity.type_id==2)
  )

  properties = db.session.scalars(stmt)

  return render_template('properties.html', properties=properties)

"""
  Select a property and all values assigned.
"""
@app.route("/property-group/<int:id>")
def property (id=None):
  g.current_language = CURRENT_LANGUAGE
  
  if not id:
    abort(400)
  else:
    property = db.session.get(Entity, f'P{id}')#db.session.scalar(select(Property).where(Property.id==f'P{id}'))
    
    #SELECT object_id, COUNT(subject_id) as `count` FROM Item_claim WHERE property_id = '10' GROUP BY object_id ORDER BY count DESC

    stmt = (
      select(func.count(EntityClaim.subject_id).label('count'))
      .where(EntityClaim.property_id==property.id)
      .group_by(EntityClaim.object_id)
      .order_by(EntityClaim.object_id)
    )

    values_counted = [row[0] for row in db.session.execute(stmt)]

    in_claims = db.session.scalars(
      select(EntityClaim)
      .where(EntityClaim.property_id==property.id)
      .order_by(EntityClaim.object_id)
    )

    grouped_claims = [list(claims) for _, claims in itertools.groupby(in_claims, lambda claim: claim.object_id)]
    counted_claims = sorted(zip(values_counted, grouped_claims), key=lambda r: r[0], reverse=True)

    return render_template('property-group.html', property=property, counted_claims=counted_claims)


"""
  Select an entity and its claims.
"""
@app.route("/item/<int:id>")
def item (id=None):

  g.current_language = CURRENT_LANGUAGE

  if not id:
    abort(400)
  else:
    item = db.session.get(Entity, f'Q{id}')
    # .execute(
    #   select(Entity)
    #     # .options(
    #     #   # Select claims
    #     #   selectinload(Item.claims)
    #     #   .options(
    #     #     # Select qualifiers on the claim
    #     #     selectinload(ItemClaim.qualifiers)
    #     #       # .options(
    #     #       #   # Select property & object
    #     #       #   joinedload(ItemClaimQualifier.property),
    #     #       #   joinedload(ItemClaimQualifier.object)
    #     #       # )
    #     #   )
    #     # )
    #     .where(Entity.id==f'Q{id}')
    #   ).scalar_one()
    
    # order the claim by prop id
    # use group by function
    in_claims = db.session.scalars(
      select(EntityClaim)
        .where(EntityClaim.subject_id==item.id)
        .order_by(EntityClaim.property_id)
    )
    claims = [list(claims) for _, claims in itertools.groupby(in_claims, lambda claim: claim.property_id)]

    # get what's an instanceOf
    instanceOf = [grouped_claim for grouped_claim in claims if grouped_claim[0].property.id == 'P1']
    if len(instanceOf) > 0:
      instanceOf = [el.object.label.text for el in instanceOf[0]]

    # get the reverse claim
    reverse = db.session.scalars(
      select(EntityClaim)
        .where(EntityClaim.object_id==item.id)
        .order_by(EntityClaim.property_id)
    )

    return render_template('item.html', item=item, claims=claims, instanceOf=instanceOf, reverse=reverse)


@app.route("/reverse/<int:id>")
def reverse (id=None):
  g.current_language = CURRENT_LANGUAGE

  if not id:
    abort(400)
  else:
    item = db.session.execute(
      select(Entity)
        .where(Entity.id==f'Q{id}')
    ).scalar_one()

    claims = db.session.scalars(
      select(EntityClaim)
        .where(EntityClaim.object_id==item.id)
        .order_by(EntityClaim.property_id)
    )
    # select(ItemClaim)
    #   .where(ItemClaim.subject_id==id)

    return render_template('reverse.html', claims=claims, item=item, id=id)


# only select the one with a <property> having a <value>


# Select e.*, ec.* from Item AS e left join ItemClaim AS ec ON e.id is ec.subject_id
#   where (ec.property_id is 1 and ec.object_id is 1) or (ec.property_id is 5 and ec.object_id is 9)
# 
# Item 1 - instanceof = work
# Item 1 - property = value
# Item 2 - instanceof = work
# (Item 2 - property != value)
# knows that i want to group back by Item


# Select ec.* from ItemClaim AS ec
#   GROUP Item ID
#   HAVING (ec.property_id = 1 and ec.object_id is 1) AND (ec.property_id is 5 and ec.object_id is 9)

@app.route("/items")
@app.route("/items/<int:p_id>")
@app.route("/items/<int:p_id>/<int:e_id>")
def items (p_id=None, e_id=None):
  
  g.current_language = CURRENT_LANGUAGE
  page = request.args.get('page', default = 1, type = int)

  if not p_id and not e_id:
    p_id = 1
    e_id = 1

  if p_id and e_id:
    count_stmt = (
      select(func.count())
        .select_from(EntityClaim)
        .where(
          (EntityClaim.property_id==f'P{p_id}')
          & (EntityClaim.object_id==f'Q{e_id}') 
          & (EntityClaim.subject_id not in [12])
        )
    )

    # ItemClim.qualifier(property_id=54, object_id=90)
    # .join(ItemClaimQualifier).where(ItemClaimQualifier.property_id==50).where(ItemClaimQualifier.object_id==90)
    print('fetching items...', p_id, e_id)

    stmt = (
      select(Entity)#, ItemClaimQualifier.object_id==90
        .options(
          # selectinload(Entity.claims.and_(EntityClaim.type == 'localMedia', EntityClaim.qualifiers.any((EntityClaimQualifier.property_id=='P54') & (EntityClaimQualifier.object_id=='Q90')))),
          # joinedload(Item.label.and_(TextInLanguage.language_id == language_id)),
          # joinedload(Item.description.and_(TextInLanguage.language_id == language_id)),
          joinedload(Entity.claims)
        )
        .join(EntityClaim, onclause=Entity.claims)
        # .where(Entity.type_id==1)
        .where(EntityClaim.property_id==f'P{p_id}', EntityClaim.object_id==f'Q{e_id}', EntityClaim.subject_id not in [12])
        .order_by(Entity.id)
    )

  elif p_id:
    # all items that are used as value for a property
    stmt = (
      select(EntityClaim)
        .options(
          joinedload(EntityClaim.property),
          joinedload(EntityClaim.object)
        )
      .join(Entity, onclause=EntityClaim.object)
      .where(EntityClaim.property_id==f'P{p_id}')
      .group_by(EntityClaim.object_id)
    )
    count_stmt = (
      select(func.count())
        .select_from(EntityClaim)
        .where(EntityClaim.property_id==f'P{p_id}')
    )



  # if we are requesting all items of a same type
  # same that type value for layout purposes
  if p_id == 1:
    instanceOf = db.session.get(Entity, f'Q{e_id}').label.text
  else:
    instanceOf = None

  # ItemClim.qualifier(property_id=54, object_id=90)
  # .join(ItemClaimQualifier).where(ItemClaimQualifier.property_id==50).where(ItemClaimQualifier.object_id==90)

  baseURL = '/items'
  if p_id: baseURL += '/' + str(p_id) 
  if e_id: baseURL += '/' +  str(e_id) 

  return render_template(
    'items.html',
    instanceOf = instanceOf,
    baseURL = baseURL,
    paginator=Paginator(session=db.session, stmt=stmt, count_stmt=count_stmt, page=page, url='/items/{}', page_size=50)
  )

# the page the contain the above iframe
@app.route("/")
@app.route("/index")
@app.route("/index/<int:p_id>")
@app.route("/index/<int:p_id>/<int:e_id>")
def index (p_id=None, e_id=None):

  g.current_language = CURRENT_LANGUAGE
  
  instanceOf = ""

  # if we are requesting all items of a same type
  # same that type value for layout purposes
  if p_id == 1:
    entity = db.session.get(Entity, f'Q{e_id}')
    if entity:
      instanceOf = entity.label.text

  baseURL = '/items'
  if p_id: baseURL += '/' + str(p_id) 
  if e_id: baseURL += '/' +  str(e_id) 

  return render_template(
    'index.html',
    instanceOf=instanceOf,
    baseURL = baseURL,
  )


# @app.route('/search', methods=["GET", "POST"])
# def search ():
#   props = {
#     'format': 16,
#     'material': 23,
#   }

#   q =  f'''SELECT DISTINCT ?prop ?wbtype ?propLabel ?propDescription ?value ?valueLabel ?valueDescription WHERE {{
#     BIND (wd:P{ props['format'] } AS ?prop) .
#     ?prop wikibase:propertyType ?wbtype.
#     # Used to extract the label
#     ?subject wdt:P{ props['format'] } ?value .
#     SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
#   }}'''

#   taxa = cached_query(q)

#   results = []

#   if request.method == 'POST':
#     # request.form is ImmutableMultidict 
#     # https://werkzeug.palletsprojects.com/en/3.0.x/datastructures/#werkzeug.datastructures.ImmutableMultiDict
#     # With property urls as keys and list of Item ids as values
#     # Treat both keys and values to make them easier to read and use them in queries
    
#     queryFilterParts = []

#     for propUrl, ItemUrls in request.form.lists():
#       prop = getPropertyId(propUrl)
#       items = [f'wd:Q{ getItemId(url) }' for url in ItemUrls ]
#       ItemString = ', '.join(items)
#       queryFilterParts.append(f'?result wdt:P{ prop } ?{ prop }. FILTER(?{ prop } IN({ ItemString }))')

#     if queryFilterParts:
#       print(queryFilterParts)
#       queryFilterString = '\n'.join(queryFilterParts)
#       print(queryFilterString)

#       q = f'''
#         SELECT ?result ?resultLabel ?resultDescription WHERE {{
#           { queryFilterString }
#           SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
#         }}
#       '''

#       print(q)

#       results = cached_query(q)

#   return render_template('search.html', taxa=taxa, results=results, activeFilters=dict(request.form.lists()))

@app.cli.command("create-item")
@click.argument("qid")
def createItem (qid):
  getItem(qid)
  db.session.flush()
  db.session.commit()


@app.cli.command("create-test-data")
def createTestData ():
  g.current_language = CURRENT_LANGUAGE
  try:
    from label.testdata import ids
    import random
    
    random.shuffle(ids)

    if db.session.query(Language).count() == 0:
      makeLanguages()

    if db.session.query(EntityType).count() == 0:
      makeEntityTypes()

    for qid in ids[:50]:
      getItem(qid)
      db.session.flush()
      db.session.commit()
  except ImportError:
    print('Could not load test data')
