from urllib.parse import quote as urlquote

from django import forms
from django.contrib import admin, messages
from django.contrib.admin.utils import quote
from django.db.models.query import QuerySet, Prefetch
from django.http import HttpResponseRedirect
from django.http.request import HttpRequest  
from django.template.response import TemplateResponse
from django.urls import path, reverse
from django.utils.html import format_html
from django.utils.translation import gettext as _
from label.models import Entity, EntityClaim, EntityClaimQualifier, \
  EntityClaimReference, EntityClaimReferenceClaim, EntityTranslation, \
  FlatPage, FlatPageContent, FlatPageSlug, FlatPageTitle, FlatPageTranslation, \
  Item, Language, Property, \
  annotate_translation, languageMap
import nested_admin

from label.wb_utils import wbi, NonExistentEntityError, deepenEntity, makeEntity

PREFERRED_LANGUAGE = 3
FALLBACK_LANGUAGE = 1

class EntityClaimReferenceClaimInline (nested_admin.NestedStackedInline):
    model = EntityClaimReferenceClaim
    verbose_name = 'claim'
    extra = 1
    fields = [('property', 'object', 'text'), 'type']
    # fields = ['type']
    autocomplete_fields = ['property', 'object']
    classes = ['entity-claim-reference-claim']


class EntityClaimReferenceInline (nested_admin.NestedStackedInline):
    model = EntityClaimReference
    verbose_name = 'reference'
    extra = 0
    classes = ['collapse', 'entity-claim-reference']
    inlines = [EntityClaimReferenceClaimInline]


class EntityClaimQualifierInline (nested_admin.SortableHiddenMixin, nested_admin.NestedStackedInline):
    fk_name = 'subject'
    model = EntityClaimQualifier
    sortable_field_name = 'order'

    class Media:
      css = {
         'all': ['label/admin/css/claim-qualifier-inline.css']
      }
      js = [
         'admin/js/jquery.init.js',
         'label/admin/js/claim-qualifier-inline.js'
      ]

    extra = 0
    # fields = [('property', 'object', 'text'), 'type', 'order']
    fields = ['type', 'order']
    autocomplete_fields = ['property', 'object']
    classes = ['collapse', 'entity-claim-qualifiers']


class EntityClaimInline (nested_admin.NestedStackedInline):
    fk_name = 'subject'
    model = EntityClaim
    inlines = [EntityClaimQualifierInline, EntityClaimReferenceInline]
    
    # def get_queryset(self, request):
    #     return super().get_queryset(request).prefetch_related(Prefetch('qualifiers'), Prefetch('references'))
    
    class Media:
      css = {
         'all': ['label/admin/css/claim-inline.css']
      }
      js = [
         'admin/js/jquery.init.js',
         'label/admin/js/claim-inline.js'
      ]

    extra = 0
    fields = [('property', 'object', 'text'), 'type']
    # fields = ['type']
    autocomplete_fields = ['property', 'object']
    classes = ['collapse', 'entity-claims']


class LabelTranslationInline (nested_admin.NestedStackedInline):
    model = EntityTranslation
    verbose_name_plural = 'Label'
    extra = 0
    fields = ['language', 'text']
    classes = ['collapse', 'translation-inline']

    class Media:
      js = [
         'admin/js/jquery.init.js',
         'label/admin/js/translation-inline.js'
      ]

    def get_queryset(self, request: HttpRequest):
        return super().get_queryset(request).filter(selected=True, field=EntityTranslation.EntityTranslationField.LABEL)


class DescriptionTranslationInline (nested_admin.NestedStackedInline):
    model = EntityTranslation
    verbose_name_plural = 'Description'
    extra = 0
    fields = ['language', 'text']
    classes = ['collapse', 'translation-inline']

    class Media:
      js = [
         'admin/js/jquery.init.js',
         'label/admin/js/translation-inline.js'
      ]

    def get_queryset(self, request: HttpRequest):
        return super().get_queryset(request).filter(selected=True, field=EntityTranslation.EntityTranslationField.DESCRIPTION)


class EntityImportForm (forms.Form):
   identifier = forms.CharField(required=True)


class EntityDeepenForm (forms.Form):
   identifier = forms.CharField(required=True)


@admin.register(Entity)
class EntityAdmin(nested_admin.NestedModelAdmin):
    search_fields = ['label', 'description']
    readonly_fields = ('deepen', 'type', 'label', 'description')
    exclude = ['shallow']
    list_display = ['__str__', 'shallow']
    inlines = (LabelTranslationInline, DescriptionTranslationInline, EntityClaimInline)

    def get_urls(self):
        urls = super().get_urls()
        # Register a url under the same pattern as django would do.
        my_urls = [
           path("import/", self.admin_site.admin_view(self.import_entity), name="%s_%s_import" % (self.opts.app_label, self.opts.model_name)),
           path("import-confirm/", self.admin_site.admin_view(self.import_entity_confirm), name="%s_%s_import_confirm" % (self.opts.app_label, self.opts.model_name)),
           path("<int:pk>/deepen/", self.admin_site.admin_view(self.deepen_entity), name="%s_%s_deepen" % (self.opts.app_label, self.opts.model_name)),
        ]
        return my_urls + urls

    def get_queryset(self, request):
        # Prefetched data is not used right now
        return super().get_queryset(request).prefetch_related(
          'claims',
          Prefetch('claims__property', annotate_translation(Entity.objects)),
          Prefetch('claims__object', annotate_translation(Entity.objects)),
          'claims__qualifiers',
          Prefetch('claims__qualifiers__property', annotate_translation(Entity.objects)),
          Prefetch('claims__qualifiers__object', annotate_translation(Entity.objects)),
          'claims__references'
        )

    def has_delete_permission(*args, **kwargs):
      return False

    def get_search_results(self, request, queryset, search_term):
      if search_term:
        hits = set([r['entity_id'] for r in EntityTranslation.objects.filter(selected=True, text__icontains=search_term).values('entity_id')])
        return queryset.filter(pk__in=hits), False
      else:
         return queryset, False


    def deepen (self, entity):
      if entity:
        if entity.shallow:
          return format_html(
            '<a href="{url_deepen}">Shallow entity. Deepen by importing claims from wikidata.</a>',
            **{
              "url_deepen": reverse(
                "admin:%s_%s_deepen" % (self.opts.app_label, self.opts.model_name),
                args=(quote(entity.pk),),
                current_app=self.admin_site.name,
              ) 
            } 
          )
        else:
          return 'Claims have already been imported from wikidata.'
      else:
        return ''

    #
    #
    # Additional views

    # Import entity view
    def import_entity(self, request):

      if request.method == 'POST':
        # Construct form
        form = EntityImportForm(request.POST)

        if form.is_valid():
          identifier = form.cleaned_data['identifier']
          
          msg_dict = {
            'identifier': identifier
          }

          if not Entity.objects.filter(identifier=identifier).exists():
            # Try to fetch object from wikidata
            try:
              if identifier.startswith('P'):
                wb_entity = wbi.property.get(identifier)
              else:
                wb_entity = wbi.item.get(identifier)

              # Get label and descriptions to show in confirm
              translations = [{
                'language': language.name,
                'label':  wb_entity.labels.get(language.code),
                'description': wb_entity.descriptions.get(language.code)
              } for language in Language.objects.all()]

              context = {
                **self.admin_site.each_context(request),
                'identifier': identifier,
                'translations': translations,
                # Url for import confirmation action
                'url_action': reverse(
                  "admin:%s_%s_import_confirm" % (self.opts.app_label, self.opts.model_name),
                  current_app=self.admin_site.name
                ),
                # Url to regular import (could probably also be the request url)
                'url_cancel': reverse(
                  "admin:%s_%s_import" % (self.opts.app_label, self.opts.model_name),
                  current_app=self.admin_site.name
                )
              }

              return TemplateResponse(request, "admin/import_entity_confirm.html", context)
              
            except NonExistentEntityError:
              msg = _("Could not find an entity with identifier “{identifier}”.")
              self.message_user(request, format_html(msg, **msg_dict), messages.ERROR)

          else:
            entity = Entity.objects.filter(identifier=identifier).only('pk', 'shallow').first()
            redirect_url = reverse(
              "admin:%s_%s_change" % (self.opts.app_label, self.opts.model_name),
              args=(quote(entity.pk),),
              current_app=self.admin_site.name,
            )

            msg = _("The entity “{identifier}” already exists.")
            self.message_user(request, format_html(msg, **msg_dict), messages.INFO)
            
            return HttpResponseRedirect(redirect_url)

      else:
        form = EntityImportForm()

      context = {
        # Include common variables for rendering the admin template.
        **self.admin_site.each_context(request),
        form: form
      }

      return TemplateResponse(request, "admin/import_entity.html", context)


    def import_entity_confirm(self, request):
      redirect_url = reverse(
        "admin:%s_%s_import" % (self.opts.app_label, self.opts.model_name),
        current_app=self.admin_site.name,
      )
      
      msg_dict = {
        "name": self.opts.verbose_name
      }

      if request.method == 'POST':
        form = EntityImportForm(request.POST)
        if form.is_valid():
          identifier = form.cleaned_data['identifier']
          
          msg_dict = {
             **msg_dict,
             "identifier": identifier
          }

          if not Entity.objects.filter(identifier=identifier).exists():
            entity = makeEntity(identifier)
            if entity:
              # Created entity
              entity_url = reverse(
                "admin:%s_%s_change" % (self.opts.app_label, self.opts.model_name),
                args=(quote(entity.pk),),
                current_app=self.admin_site.name,
              )

              # Add a link to the object's change form if the user can edit the obj.
              if self.has_change_permission(request, entity):
                entity_repr = format_html('<a href="{}">{}</a>', urlquote(entity_url), entity)
              else:
                entity_repr = str(entity)

              msg_dict = {
                **msg_dict,
                "entity": entity_repr
              }

              msg = _("The {name} “{entity}” was imported successfully.")

              if "_importanother" in request.POST:
                self.message_user(request, format_html(msg, **msg_dict), messages.SUCCESS)
              elif self.has_change_permission(request, entity):
                # Redirect to imported entity
                msg += " " + _("You may edit it again below.")
                self.message_user(request, format_html(msg, **msg_dict), messages.SUCCESS)

                return HttpResponseRedirect(entity_url)
            
            else:
              # Object could not be created
              msg = _("The entity “{identifier}” could not be imported.")
              self.message_user(request, format_html(msg, **msg_dict), messages.ERROR)
          else:
            entity = Entity.objects.filter(identifier=identifier).only('pk', 'shallow').first()
            # Update redirect url to entity page
            redirect_url = reverse(
              "admin:%s_%s_change" % (self.opts.app_label, self.opts.model_name),
              args=(quote(entity.pk),),
              current_app=self.admin_site.name,
            )

            msg = _("The {name} “{identifier}” already exists.")
            self.message_user(request, format_html(msg, **msg_dict), messages.INFO)
      else:
        msg = _("Did not receive post request")
        self.message_user(request, format_html(msg, **msg_dict), messages.ERROR)

      # By default return to import view, either with a success message
      # or an error
      return HttpResponseRedirect(redirect_url)


    def deepen_entity (self, request, pk):
      try:
        entity = Entity.objects.get(pk=pk)
        entity_url = reverse(
          "admin:%s_%s_change" % (self.opts.app_label, self.opts.model_name),
          args=(quote(entity.pk),),
          current_app=self.admin_site.name,
        )
        entity_repr = str(entity)

        msg_dict = {
          "identifier": entity.identifier,
          "entity": entity_repr
        }

        if not entity.shallow:
          msg = _("Claims have already been imported for {entity}.")
          self.message_user(request, format_html(msg, **msg_dict), messages.INFO)
          return HttpResponseRedirect(entity_url)
        elif request.method == 'POST':
          # Fill entity
          # On confirm redirect to entity
          if (deepenEntity(entity)):
            msg = _("Claims have been successfully deepened for {entity}.")
            self.message_user(request, format_html(msg, **msg_dict), messages.INFO)
            return HttpResponseRedirect(entity_url)
          else:
            msg = _("Could import claims for {entity}.")
            self.message_user(request, format_html(msg, **msg_dict), messages.ERROR)
            return HttpResponseRedirect(reverse(
              "admin:%s_%s_deepen" % (self.opts.app_label, self.opts.model_name),
              args=(quote(entity.pk),),
              current_app=self.admin_site.name,
            ))
        else:
          # Show confirm dialog
          form = EntityDeepenForm()

          context = {
            # Include common variables for rendering the admin template.
            **self.admin_site.each_context(request),
            "form": form,
            "entity": entity,
            "identifier": entity.identifier,
            "url_cancel": entity_url
          }

          return TemplateResponse(request, "admin/deepen_entity.html", context)
      except Entity.DoesNotExist:
        # Does not exist, redirect to listing
        msg = _("Could not find the entity.")
        self.message_user(request, format_html(msg, **msg_dict), messages.ERROR)
        return HttpResponseRedirect(reverse(
          "admin:%s_%s_changelist" % (self.opts.app_label, self.opts.model_name),
          current_app=self.admin_site.name,
        ))


@admin.register(Item)
class ItemAdmin (nested_admin.NestedModelAdmin):
    readonly_fields = ('label', 'description')
    # exclude = ('label', 'description')

    def get_search_results(self, request, queryset, search_term):
        if search_term:
          hits = set([r['entity_id'] for r in EntityTranslation.objects.filter(selected=True, text__icontains=search_term).values('entity_id')])
          return queryset.filter(pk__in=hits), False
        else:
           return queryset, False
    

@admin.register(Property)
class PropertyAdmin (nested_admin.NestedModelAdmin):
    readonly_fields = ('label', 'description')
    search_fields = ['label', 'description']

    inlines = (EntityClaimInline,)

    def has_delete_permission(*args, **kwargs):
      return False

    def get_search_results(self, request, queryset, search_term):
        if search_term:
          hits = set([r['entity_id'] for r in EntityTranslation.objects.filter(selected=True, text__icontains=search_term).values('entity_id')])
          return queryset.filter(pk__in=hits), False
        else:
           return queryset, False


class FlatPageTranslationInlineAbstract (nested_admin.NestedStackedInline):
    model = FlatPageTranslation
    extra = 0
    fields = ['language', 'text']
    classes = ['collapse', 'translation-inline']
    verbose_name_plural = None
    translation_field = None

    class Media:
      js = [
         'admin/js/jquery.init.js',
         'label/admin/js/translation-inline.js'
      ]

    def get_queryset(self, request: HttpRequest):
        return super().get_queryset(request).filter(selected=True, field=self.translation_field)


class FlatPageTranslationInlineSlug (FlatPageTranslationInlineAbstract):
    model = FlatPageSlug
    verbose_name_plural = 'Slug'
    translation_field = FlatPageTranslation.FlatPageTranslationField.SLUG


class FlatPageTranslationInlineTitle (FlatPageTranslationInlineAbstract):
    model = FlatPageTitle
    verbose_name_plural = 'Title'
    translation_field = FlatPageTranslation.FlatPageTranslationField.TITLE


class FlatPageTranslationInlineContent (FlatPageTranslationInlineAbstract):
    model = FlatPageContent 
    verbose_name_plural = 'Content'
    translation_field = FlatPageTranslation.FlatPageTranslationField.CONTENT


@admin.register(FlatPage)
class FlatPageAdmin(nested_admin.NestedModelAdmin):
    list_display = ['name',]
    inlines = (FlatPageTranslationInlineSlug, FlatPageTranslationInlineTitle, FlatPageTranslationInlineContent)