from django import template
import re
import datetime
from label.models import FlatPage
from django.template import Template, Context
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter(name="get")
def get (entity, attribute):
  return getattr(entity, attribute)

@register.filter(name="fileOriginalURL")
def fileOriginalURL (url):
  return url.replace("wiki/File:","wiki/Special:Redirect/file/")

@register.filter(name="instanceOf")
def instanceOf (entity):
  return [ claim.object.identifier for claim in entity.instanceOf]

@register.filter(name="instanceOfBad")
def instanceOfBad (entity):
  return [ claim.object.identifier for claim in entity.claims.all() if claim.property.identifier == 'P31']

@register.filter(name="anyinstanceOfBad")
def anyinstanceOfBad (grouped_claim):
  entities = [claim.target for claim in grouped_claim]
  result = []
  for entity in entities:
    result += [ claim.object.identifier for claim in entity.claims.all() if claim.property.identifier == 'P31' ]
  return result


"""
  Returns groups of claims for given properties.
"""
@register.filter(name="findPropertiesInGroupedClaims")
def findPropertiesInGroupedClaims (groupedClaims, searchProperties):
  searchProperties = set(map(str.strip, searchProperties.split(',')))
  found = []

  for claims in groupedClaims:
    print(claims[0].property.identifier, claims[0].property.identifier in searchProperties)
    if claims[0].property.identifier in searchProperties:
      print(claims[0].text)
      found.append(claims)

  print(found)
  return found

@register.filter(name="isDate")
def isDate (val):
  return re.match('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', val)

@register.filter(name="asDate")
def asDate (val):
  date = datetime.datetime.strptime(val, '%Y-%m-%d %H:%M:%S')
  return date.strftime('%d-%m-%Y')

@register.simple_tag
def find_page (name):
  try:
    page = FlatPage.objects.get(name=name, published=True)
    return page
  except FlatPage.DoesNotExist:
    return None
  

@register.filter
def render_as_template (template_source):
  return mark_safe(Template(template_source).render(Context({})))