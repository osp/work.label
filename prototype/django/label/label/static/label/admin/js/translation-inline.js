// Change language selection field to read only divs on language inlines.
// Doing it in the backend seems neigh impossible:
// https://code.djangoproject.com/ticket/15602
django.jQuery(document).ready(function () {
  django.jQuery('.translation-inline .has_original .field-language').each(function () {
    let $this = django.jQuery(this);
    let select = $this.find('select').first();
    let hidden = django.jQuery('<input>', {
        'type': 'hidden',
        'id': select.attr('id'),
        'name': select.attr('name'),
        'data-context': select.attr('data-context'),
        'value': select.val() 
    });
    let readonly = django.jQuery('<div>', {'class': 'readonly'});
    readonly.text(select.find(':selected').text());
    readonly.append(hidden);
    select.replaceWith(readonly);
  });
});