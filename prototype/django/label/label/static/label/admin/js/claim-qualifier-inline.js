django.jQuery(document).ready(function () {
  django.jQuery('fieldset.entity-claim-qualifiers [data-inline-model="label-entityclaimqualifier"].djn-item.has_original').each(function () {
    let $this = django.jQuery(this);
    let objectCell = $this.find('> fieldset div.fieldBox.field-object').first();
    let textCell = $this.find('> fieldset div.fieldBox.field-text').first();
    let type = $this.find('> fieldset div.field-type input').first().val();
  
    if (type == 'wikibase-item' || type == 'wikibase-item') {
      textCell.hide();
    }
    else {
      objectCell.hide();
    }
  });
});