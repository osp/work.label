from django.core.management.base import BaseCommand
from label.wb_utils import getItem

from label.models import Language

def makeLanguages ():
    Language(code='en', name='English').save()
    Language(code='nl', name='Dutch').save()
    Language(code='fr', name='French').save()

# print(getItem('Q6187'))

class Command (BaseCommand):
    args = ''
    help = 'Fetch test data from a wikibase instance'

    def handle (self, *args, **options):
        try:
            from label.management.commands.testdata import ids
            
            # random.shuffle(ids)

            if not Language.objects.exists():
                makeLanguages()

            for qid in ids:
                print("Trying to get ", qid)
                getItem(qid)
        except ImportError:
            print('Could not load test data')