from django.conf import settings
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.clickjacking import xframe_options_exempt
import itertools
from .models import Entity, Item, Property, EntityClaim, EntityTranslation, annotate_translation, FlatPage, FlatPageSlug, languageMap
from django.db.models import Prefetch, Subquery
from django.db.models import Case, Prefetch, Subquery, Value, When
from django.db.models import Q, Count
from django.core.paginator import Paginator
from django.utils.translation import get_language
from django.http import Http404
import markdown
import re


# those are the properties that are shown in the summary form of an item
synthesis_properties = [
    ('genre', 'P136'),
    ('media', 'P12548'),
    ('fabrication', 'P2079'),
    ('software', 'P408'),
    ('programed in', 'P277'),
    ('language', 'P407'),
    ('contributors', 'P50, P170, P767'),
    ('date', 'P577, P571'),
]

# those are needed to be prefetched for display purposes
additionnal_properties = [
    ('instanceOf', 'P31'),

    # synonims stay separate in the claims list, with their different name
    # but groups under a common name for display purposes in synthesis view
    ('urls', 'P856, P2699, P973')
]

# properties for which it makes sense to display the desciptions of their subject
descriptive_properties = [

]

# this is the total list of properties that should be prefetched on an item
prefectch_properties = synthesis_properties + additionnal_properties


"""
    UTILITY FUNCTIONS
    ===========================================================
"""

"""
    Prefetches related claims and translations of their objects.
"""
def map_translated_claims_to (attribute, *args, **kwargs):
    # Prefetch of claims
    return Prefetch(
        'claims',
        queryset=EntityClaim.objects.filter(*args, **kwargs).prefetch_related(
            # Prefetch related objects and their translations
            Prefetch('object', queryset=annotate_translation(Entity.objects)),
            Prefetch('property', queryset=annotate_translation(Entity.objects))
        ), to_attr=attribute)

def list_all_properties():
    # we show properties whenever there is at least 2 values
    return Property.objects.annotate(
        amount = Count("property_in_claims__object_id", distinct=True)
        ).filter(amount__gt = 1).order_by('-amount')

def create_claims_group(claims, reverse = False):
    grouped_claims = [list(claims) for _, claims in itertools.groupby(claims, lambda claim: claim.property_id)]
    for grouped_claim in grouped_claims:
        for claim in grouped_claim:
            if reverse:
                claim.target = claim.subject
            else:
                claim.target = claim.object

            if claim.target:
                # this one kills any optimisation, figure out how to clean
                to_prefetch = []
                for p in additionnal_properties:
                    ids = p[1].split(', ')
                    to_prefetch.append(map_translated_claims_to(p[0], property__identifier__in = ids))
                claim.target = Item.objects.filter(pk=claim.target.pk).prefetch_related(*to_prefetch).first()

    return grouped_claims


"""
    VIEWS
    ===========================================================
"""

def entity (request, pk):

    to_prefetch = []
    for p in additionnal_properties:
        ids = p[1].split(', ')
        to_prefetch.append(map_translated_claims_to(p[0], property__identifier__in = ids))

    if pk.startswith('P') or pk.startswith('Q'):
        entity = get_object_or_404(Entity.objects.prefetch_related(*to_prefetch), identifier=pk)
    else:
        entity = get_object_or_404(Entity.objects.prefetch_related(*to_prefetch), pk=pk)

    # claims grouped by properties
    claims = entity.claims.all().order_by('property_id').prefetch_related(
        Prefetch('object', queryset=annotate_translation(Entity.objects)),
        Prefetch('property', queryset=annotate_translation(Entity.objects)),
        'object__claims')
    grouped_claims = create_claims_group(claims)

    # separate between top and others
    top_grouped_claims = []
    top_ids = ', '.join([ p[1] for p in synthesis_properties ])
    for grouped_claim in grouped_claims:
        if grouped_claim[0].property.identifier in top_ids:
            top_grouped_claims.append(grouped_claim)
    for top_grouped_claim in top_grouped_claims:
        grouped_claims.remove(top_grouped_claim)

    # separate between entity and text
    text_grouped_claims = []
    for grouped_claim in grouped_claims:
        if not grouped_claim[0].type == 'wikibase-item':
            text_grouped_claims.append(grouped_claim)
    for text_grouped_claim in text_grouped_claims:
        grouped_claims.remove(text_grouped_claim)

    # reverse claims grouped by properties
    reverse_claims = EntityClaim.objects.filter(object_id = entity.pk).order_by('property_id')\
        .prefetch_related(
            Prefetch('subject', queryset=annotate_translation(Entity.objects)),
            Prefetch('property', queryset=annotate_translation(Entity.objects)),
            'subject__claims')
    grouped_reverse_claims = create_claims_group(reverse_claims, True)

    instanceOf = [ claim.object for claim in claims if claim.property.identifier == 'P31' and claim.object ]
    is_elit = 'Q173167' in [ o.identifier for o in instanceOf ]

    return render(
        request,
        "entity.html",
        {
            'entity': entity,
            'top_grouped_claims': top_grouped_claims,
            'grouped_claims': grouped_claims,
            'text_grouped_claims': text_grouped_claims,
            'grouped_reverse_claims': grouped_reverse_claims,
            'instanceOf': instanceOf,
            'is_elit': is_elit,
            'properties': list_all_properties(),
        }
    )


@xframe_options_exempt
def values (request, pk):

    page = int(request.GET.get('page', '1'))

    # get all of its values
    qs = Subquery(EntityClaim.objects.filter(property_id = pk).values('object_id'))
    entities = Paginator(Entity.objects.filter(pk__in=qs).prefetch_related(
        map_translated_claims_to("instanceOf", property__identifier = 'P31'),
    ), 20)

    pages = [ i + 1 for i in range(entities.num_pages) ]
    entities = entities.page(page)

    return render(
        request,
        "entities.html",
        {
            'entities': entities,
            'page': page,
            'pages': pages,
        }
    )


@xframe_options_exempt
def entities (request, query='P31-Q173167'):
    # instance of (P31)
    # electronic literature (Q173167)

    # GET parameter
    search_term = request.GET.get('search_term', None)
    page = int(request.GET.get('page', '1'))
    homepage = True if 'homepage' in request.GET else False

    if query:
        filters = [ t.split('-') for t in query.split('_') ]
        qs = None
        for prop, val in filters:
            if not qs:
                qs = EntityClaim.objects.filter(property__identifier = prop, object__identifier = val).values('subject_id')
            else:
                qs = EntityClaim.objects.filter(property__identifier = prop, object__identifier = val, subject_id__in = Subquery(qs)).values('subject_id')
        # print(qs)
        query_qs = [d['subject_id'] for d in list(qs)]

    if search_term:
        # Text search
        qs = Subquery(EntityTranslation.objects.filter(selected=True, text__icontains=search_term).values('entity_id'))
        qs_as_subject = EntityClaim.objects.filter(object_id__in = qs).values_list('subject_id')
        qs = qs_as_subject.union(qs)
        # Do not include reverse links because this yields too much results.
        # qs_as_object = EntityClaim.objects.filter(subject_id__in = qs).values_list('object_id')
        search_qs = [d[0] for d in list(qs)]

    # intersection of both pk set
    if search_term and query:
        qs = [pk for pk in search_qs if pk in query_qs]

    # Only list Item in search, and not the properties themselves 
    to_prefetch = []
    for p in prefectch_properties:
        ids = p[1].split(', ')
        to_prefetch.append(map_translated_claims_to(p[0], property__identifier__in = ids))
    entities = Item.objects.filter(pk__in=qs).prefetch_related(*to_prefetch)
    
    # test if at least one item is elit
    elit = False
    for e in entities:
        # print(e.instanceOf)
        if 'Q173167' in [v.object.identifier for v in e.instanceOf]:
            elit = True
    
    entities = Paginator(entities, 20)

    pages = [ i + 1 for i in range(entities.num_pages) ]
    entities = entities.page(page)

    return render(
        request,
        "entities.html",
        {
            'entities': entities,
            'page': page,
            'pages': pages,
            'elit': elit,
            'synthesis_properties': synthesis_properties, 
            'homepage': homepage,
        }
    )

def index (request, query=None):
    homepage = False

    if not query:
        query = 'P31-Q173167'
        homepage = True

    # remove duplicate in query
    if query:
        query = "_".join(list(dict.fromkeys(query.split('_'))))

    search_term = request.GET.get('search_term', None)

    filters = [ t.split('-') for t in query.split('_') ]
    
    # make sub-header
    subheader = {
        'filter': [],
        'search': search_term,
    }
    for prop, val in filters:
        if prop == 'P31':
            subheader['filter'].append(Entity.objects.get(identifier=val))

    # all the value for 'instanceOf' where elit is also used
    elit_qs = EntityClaim.objects.filter(property__identifier = 'P31', object__identifier = 'Q173167').values('subject_id')
    forms_qs = EntityClaim.objects.filter(subject_id__in = Subquery(elit_qs), property__identifier = 'P31').values('object_id')
    
    # 1. only count the claim where the object is elit
    # 2. only count the claim where the object where it's used with instanceOf
    # ex: 'game' is used with 'instanceOf' but also with 'genre'
    instanceOf = Entity.objects.filter(pk__in=Subquery(forms_qs)).annotate(
        amount = Count("object_in_claims", #all the claims that uses that forms
            filter= Q(object_in_claims__property__identifier = 'P31') & Q(object_in_claims__subject_id__in = Subquery(elit_qs)),
            distinct=True)
    ).order_by('-amount')

    return render(
        request,
        "index.html",
        {
            'query': query,
            'subheader': subheader,
            'homepage': homepage,
            'search_term': search_term,
            'instanceOf': instanceOf,
            'properties': list_all_properties(),
        }
    )

def about (request):
    return render(request, "about.html")

def page (request, slug):
    # Select pages. Query in translation of slug
    # TranslationSlug.find() < with preference for current language
    preferred_language = languageMap.get(get_language())

    try:
        slug = FlatPageSlug.objects.select_related('page').get(text=slug, language_id=preferred_language, page__published=True)
        selected_page = slug.page
    
    except FlatPageSlug.DoesNotExist:
        slug = FlatPageSlug.objects.select_related('page').get(text=slug, page__published=True)
        translations = FlatPageSlug.objects.filter(page_id=slug.page.pk).order_by(
            Case(
                When(language_id=languageMap.get(get_language()), then=Value(1)),
                When(language_id=languageMap.get(settings.FALLBACK_LANGUAGE), then=Value(2)),
                default=Value(3))
        )[:1]

        if len(translations) > 0:
            if translations[0].text != slug:
                return redirect(page, translations[0].text)
            else:
                selected_page = translations[0].page
        else:
            raise Http404('Page not found.')

    # If slug is found with different language, check whether one
    # exists in current language and redirect
    # else render
    return render(request, "page.html", {
        'slug': slug,
        'title': selected_page.title,
        'content': selected_page.content
    })






