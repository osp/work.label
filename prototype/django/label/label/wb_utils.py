import logging

from wikibaseintegrator import WikibaseIntegrator
from wikibaseintegrator.wbi_config import config as wbi_config
from wikibaseintegrator.wbi_enums import WikibaseDatatype
from wikibaseintegrator.wbi_exceptions import MissingEntityException, NonExistentEntityError

from label.models import Entity, EntityClaim, EntityClaimQualifier, \
    EntityClaimReference, EntityClaimReferenceClaim, EntityTranslation, Language

from label.utils import format_date

import time
import datetime


wbi_config['USER_AGENT'] = 'LabEL data fetcher/0.0.1a (admin@osp.kitchen) Wikibase Integrator/0.12'
wbi_config['MEDIAWIKI_API_URL'] = 'https://www.wikidata.org/w/api.php'
wbi_config['SPARQL_ENDPOINT_URL'] = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'
wbi_config['WIKIBASE_URL'] = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'

wbi = WikibaseIntegrator()


logging.basicConfig(level=logging.DEBUG)

LOCAL_MEDIA_PREFIX = '/'
COMMONS_MEDIA_PREFIX = '/'

WIKIBASE_ITEM_ID = 'wikibase-itemid'
WIKIBASE_ENTITY_ID = 'wikibase-entityid'

MAX_DEPTH = 1

# def format_date (date):
#     if date.precision == YEAR:
#         date.date.strftime('%Y')
#     elif date.precision == MONTH:
#         date.date.strftime('%m-%Y')
#     elif date.precision == DAY:
#         date.date.strftime('%d-%m-%Y')


def parseStringValue (value):
  # "Speculative futures cover.png"
  return value, None

def parseItemIdValue (value):
    # {"item-type": "item", "numeric-id": 68, "id": "Q68"}
    return value['id'], None


def parseMonoLingualTextValue (value):
    # {"text": "Dream Babes: Speculative Futures", "language": "en"}
    return value['text'], None


def parseQuantityValue (value):
    # Amount is either a number, or a number with a unit.
    # Units themselves are Wikibase items.
    # Create a list somewhere of units. Also for data-
    # {"amount": "+2000", "unit": "1"}, "type": "quantity"}
    # { "amount": "+130", "unit": "https://daap.bannerrepeater.org/entity/Q38" }
    if '.' in value['amount']:
        return float(value['amount']), value
    else:
        return int(value['amount']), value
        

def parseTimeValue (value):
    # Dates can be encoded with a different pattern than
    # their precision suggests.
    # @FIXME: construct which includes precision for the template.
    patterns = [
        '+%Y-%m-%dT00:00:00Z',
        '+%Y-%m-00T00:00:00Z',
        '+%Y-00-00T00:00:00Z'
    ]
    for pattern in patterns:
        try:
            date = datetime.datetime.strptime(value['time'], pattern)
            extra_data = {
                'timestamp': date.timestamp(),
                'precision': value['precision']
            }
            label = format_date(date, value['precision'])

            return label, extra_data
        except ValueError:
            continue

    # # {"time": "+2017-03-15T00:00:00Z", "timezone": 0, "before": 0, "after": 0, "precision": 11, "calendarmodel": "http://www.wikidata.org/entity/Q1985727"}
    # if value['precision'] == WikibaseDatePrecision.DAY.value:
    #     return datetime.datetime.strptime(value['time'], '+%Y-%m-%dT00:00:00Z')
    # if value['precision'] == WikibaseDatePrecision.MONTH.value:
    #     return datetime.datetime.strptime(value['time'], '+%Y-%m-00T00:00:00Z')
    # if value['precision'] == WikibaseDatePrecision.YEAR.value:
    #     return datetime.datetime.strptime(value['time'], '+%Y-00-00T00:00:00Z')


def parseValue(value):
    try:
        if value['type'] == WikibaseDatatype.STRING.value:
            # {"value": "Speculative futures cover.png", "type": "string"}
            return parseStringValue(value['value'])
        elif value['type'] == WIKIBASE_ENTITY_ID or value['type'] == WIKIBASE_ITEM_ID:
            # {"value": {"entity-type": "item", "numeric-id": 68, "id": "Q68"}, "type": "wikibase-entityid"}, "datatype": "wikibase-item"}
            return parseItemIdValue(value['value'])
        elif value['type'] == WikibaseDatatype.MONOLINGUALTEXT.value:
            # {"value": {"text": "Dream Babes: Speculative Futures", "language": "en"}, "type": "monolingualtext"}, "datatype": "monolingualtext"}
            return parseMonoLingualTextValue(value['value'])
        elif value['type'] == WikibaseDatatype.TIME.value:
            # {"value": {"time": "+2017-03-15T00:00:00Z", "timezone": 0, "before": 0, "after": 0, "precision": 11, "calendarmodel": "http://www.wikidata.org/entity/Q1985727"}, "type": "time"}
            return parseTimeValue(value['value'])
        elif value['type'] == WikibaseDatatype.QUANTITY.value:
            return parseQuantityValue(value['value'])
    except KeyError:
        print("Could not determine type for", value)

    print("Could not determine type for ", value)
    return None, None

def parseDatatypeItem(value):
    return parseValue(value)



def parseDatatypeTime(value):
    return parseValue(value)


def parseDatatypeLocalMedia(value):
    # Download copy of the media file?
    # Wrap in construct?
    return LOCAL_MEDIA_PREFIX + parseValue(value)


def parseDatatypeCommonsMedia(value):
    # Wrap in construct?
    # Download copy of the media file?
    return COMMONS_MEDIA_PREFIX + parseValue(value)


def parseDatatypeString (value):
    # Wrap in construct?
    return parseValue(value)


def parseDatatypeUrl (value):
    # Wrap in construct?
    return parseValue(value)


def parseDatatypeExternalId(value):
    # Wrap in construct?
    return parseValue(value)

def makeEntityClaimReference (wb_claim_reference, entity_claim, depth):
    reference = EntityClaimReference(entity_claim=entity_claim)
    reference.save()

    for wb_claim in wb_claim_reference:
        claim_prop = getProperty(wb_claim.property_number, depth)

        if claim_prop:
            value = None
            text = None
            extra_data = None

            if wb_claim.datatype == WikibaseDatatype.ITEM.value:
                value = getItem(parseDatatypeItem(wb_claim.datavalue)[0], depth)

                if value is None:
                    return None
            elif wb_claim.datatype == WikibaseDatatype.PROPERTY.value:
                value = getProperty(parseDatatypeItem(wb_claim.datavalue)[0], depth)

                if value is None:
                    return None
            elif wb_claim.datatype == WikibaseDatatype.TIME.value:
                text, extra_data = parseDatatypeTime(wb_claim.datavalue)
            else:
                text, extra_data = parseValue(wb_claim.datavalue)

            claim_qualifier = EntityClaimReferenceClaim(
                subject=reference,
                property=claim_prop,
                object=value,
                text=text,
                extra_data=extra_data,
                type=wb_claim.datatype)

            claim_qualifier.save()

    return reference

def makeEntityClaimQualifier (wb_claim_qualifier, entity_claim, order, depth=0):
    qualifier_prop = getProperty(wb_claim_qualifier.property_number, depth)
    
    if qualifier_prop:
        value = None
        text = None
        extra_data = None

        if wb_claim_qualifier.datatype == WikibaseDatatype.ITEM.value:
            value = getItem(parseDatatypeItem(wb_claim_qualifier.datavalue)[0], depth)

            if value is None:
                return None
        elif wb_claim_qualifier.datatype == WikibaseDatatype.PROPERTY.value:
            value = getProperty(parseDatatypeItem(wb_claim_qualifier.datavalue)[0], depth)

            if value is None:
                return None
        elif wb_claim_qualifier.datatype == WikibaseDatatype.TIME.value:
            text, extra_data = parseDatatypeTime(wb_claim_qualifier.datavalue)
        else:
            text, extra_data = parseValue(wb_claim_qualifier.datavalue)

        claim_qualifier = EntityClaimQualifier(
            order=order,
            subject=entity_claim,
            property=qualifier_prop,
            object=value,
            text=text,
            extra_data=extra_data,
            type=wb_claim_qualifier.datatype)

        claim_qualifier.save()
        
        return claim_qualifier
    else:
        return None

def makeEntityClaim (wb_claim, entity, depth=0):
    claim_prop = getProperty(wb_claim.mainsnak.property_number, depth)

    if claim_prop:
        value = None
        text = None
        extra_data = None

        if wb_claim.mainsnak.datatype == WikibaseDatatype.ITEM.value:
            value = getItem(parseDatatypeItem(wb_claim.mainsnak.datavalue)[0], depth)

            if value is None:
                # Could not find entity.
                return None  
        elif wb_claim.mainsnak.datatype == WikibaseDatatype.PROPERTY.value:
            value = getProperty(parseDatatypeItem(wb_claim.mainsnak.datavalue)[0], depth)
            
            if value is None:
                # Could not find entity.
                return None
        elif wb_claim.mainsnak.datatype == WikibaseDatatype.TIME.value:
            text, extra_data = parseDatatypeTime(wb_claim.mainsnak.datavalue)
        else:
            text, extra_data = parseValue(wb_claim.mainsnak.datavalue)

        claim = EntityClaim(
            subject=entity,
            property=claim_prop,
            object=value,
            text=text,
            extra_data=extra_data,
            type=wb_claim.mainsnak.datatype)

        claim.save()

        for idx, wb_claim_qualifier in enumerate(wb_claim.qualifiers):
            makeEntityClaimQualifier(wb_claim_qualifier, claim, idx, depth)

        for wb_claim_reference in wb_claim.references:
            makeEntityClaimReference(wb_claim_reference, claim, depth)

        return claim
    else:
        return None

def makeItem (qid, depth=0):
    print("Making item ", qid)
    try:
        wb_item = wbi.item.get(qid)
        
        # Fix me: look into foreign key field of label and description
        item = Entity(
            identifier=qid,
            wb_identifier=qid,
            type=Entity.EntityType.ITEM,
            shallow=(depth >= MAX_DEPTH)
        )        

        item.save()

        for language in Language.objects.all():
            wb_label_translation = wb_item.labels.get(language.code)
            if wb_label_translation:
                EntityTranslation(
                    language=language,
                    entity=item,
                    key=f'item:label:{qid}',
                    text=wb_label_translation.value,
                    field=EntityTranslation.EntityTranslationField.LABEL,
                    selected=True
                ).save()

            wb_label_description = wb_item.descriptions.get(language.code)
            if wb_label_description:
                EntityTranslation(
                    language=language,
                    entity=item,
                    key=f'item:description:{qid}',
                    text=wb_label_description.value,
                    field=EntityTranslation.EntityTranslationField.DESCRIPTION,
                    selected=True
                ).save()


        if not item.shallow:
            for wb_claim in wb_item.claims:
                # Mainsnak holds the value of the claim
                makeEntityClaim(wb_claim, item, depth)
        
        return item
    except MissingEntityException:
        print(f"Entity not found on remote {qid}")
        return None


def makeProperty (pid, depth=0):
    print("Making property", pid)
    try:
        wb_prop = wbi.property.get(pid)


        prop = Entity(
            identifier=pid,
            wb_identifier=pid,
            type=Entity.EntityType.PROPERTY,
            shallow=(depth >= MAX_DEPTH)
        )

        prop.save()

        for language in Language.objects.all():
            wb_label_translation = wb_prop.labels.get(language.code)
            if wb_label_translation:
                EntityTranslation(
                    entity=prop,
                    language=language,
                    key=f'property:label:{pid}',
                    text=wb_label_translation.value,
                    field=EntityTranslation.EntityTranslationField.LABEL,
                    selected=True
                ).save()

            wb_label_description = wb_prop.descriptions.get(language.code)
            if wb_label_description:
                EntityTranslation(
                    entity=prop,
                    language=language,
                    key=f'property:description:{pid}',
                    text=wb_label_description.value,
                    field=EntityTranslation.EntityTranslationField.DESCRIPTION,
                    selected=True
                ).save()
                

        if not prop.shallow:
            for wb_claim in wb_prop.claims:
                # Mainsnak holds the value of the claim
                # print(claim.mainsnak, claim.mainsnak.datatype, WikibaseDatatype.ITEM)
                makeEntityClaim(wb_claim, prop, depth)

        return prop
    except MissingEntityException:
        print(f"Entity not found on remote: {pid}")
        return None


def makeEntity (identifier, depth=0):
    if identifier.startswith('P'):
        return makeProperty(identifier, depth)
    else:
        return makeItem(identifier, depth)

def getItem (qid, depth=-1):
    # item = db.session.scalar(select(Entity).where(Entity.identifier==qid))
    try:
        return Entity.objects.get(identifier=qid)
    except Entity.DoesNotExist:
        time.sleep(.15)
        return makeItem(qid, depth + 1)

def getProperty (pid, depth=0):
    # Fetch it from the database.
    # If it does not it exist fetch it from the API
    # prop = db.session.scalar(select(Entity).where(Entity.identifier==pid))

    try:
        return Entity.objects.get(identifier=pid)
    except Entity.DoesNotExist:
        time.sleep(.15)
        return makeProperty(pid, depth + 1)
    

def getEntity (identifier, depth):
    if identifier.startswith('P'):
        return getProperty(identifier, depth)
    else:
        return getItem(identifier, depth)


def deepenProperty (prop, depth=0):
  wb_prop = wbi.property.get(prop.identifier)
  for wb_claim in wb_prop.claims:
    # Mainsnak holds the value of the claim
    # print(claim.mainsnak, claim.mainsnak.datatype, WikibaseDatatype.ITEM)
    makeEntityClaim(wb_claim, prop, depth)
  prop.shallow = False
  prop.save()

  return prop

def deepenItem (item, depth=0):
  wb_item = wbi.item.get(item.identifier)
  for wb_claim in wb_item.claims:
    # Mainsnak holds the value of the claim
    makeEntityClaim(wb_claim, item, depth)

  item.shallow = False
  item.save()

  return item


def deepenEntity (entity):
  if entity.identifier.startswith('P'):
    return deepenProperty(entity)
  else:
    return deepenItem(entity)