# Generated by Django 5.1.3 on 2024-12-11 16:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('label', '0003_entityclaimreference_entityclaimreferenceclaim'),
    ]

    operations = [
        migrations.AddField(
            model_name='entityclaim',
            name='extra_data',
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='entityclaimqualifier',
            name='extra_data',
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='entityclaimreferenceclaim',
            name='extra_data',
            field=models.JSONField(blank=True, null=True),
        ),
    ]
