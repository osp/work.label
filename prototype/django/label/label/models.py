from django.conf import settings
from django.db import models
from django.db.models import Case, OuterRef, Subquery, Value, When
from django.db.models.query import QuerySet
from django.utils.translation import get_language
from django.utils.safestring import mark_safe

import re
import markdown
from django.template import Template, Context


FALLBACK_LANGUAGE = 'en'

class Language (models.Model):
    code = models.CharField(max_length=10)
    name = models.TextField()

    def __str__ (self):
        return self.name


class TranslatedEntityManager (models.Manager):
    def get_queryset(self):
        # return super().get_queryset().select_related('label', 'description')

        # Use ugly field names to allow for less ugly property functions
        # which are required for the admin?
        return super().get_queryset().annotate(
            _label = select_entity_translation_subquery('pk', EntityTranslation.EntityTranslationField.LABEL),
            _description = select_entity_translation_subquery('pk',  EntityTranslation.EntityTranslationField.DESCRIPTION)
        )


class Entity (models.Model):
    class Meta:
        verbose_name_plural = "entities"

    class EntityType(models.IntegerChoices):
        ITEM = 0
        PROPERTY = 1

    objects = TranslatedEntityManager()

    # Consider to put it in an external table to
    # have more remote identifiers?
    identifier = models.CharField(max_length=250)
    wb_identifier = models.CharField(max_length=250)
    shallow = models.BooleanField(default=False)
    
    type = models.IntegerField(choices=EntityType)
    data_type = models.CharField(max_length=50, blank=True)

    # label = models.ForeignKey(Term, on_delete=models.PROTECT, related_name='item_label', blank=True, null=True)
    # description = models.ForeignKey(Term, on_delete=models.PROTECT, related_name='item_description', blank=True, null=True)

    @property
    def label (self):
        if not hasattr(self, '_label'):
            self._label = select_entity_field_translation(self.pk, EntityTranslation.EntityTranslationField.LABEL)

        return self._label

    @property
    def description (self):
        if not hasattr(self, '_description'):
            self._description = select_entity_field_translation(self.pk, EntityTranslation.EntityTranslationField.DESCRIPTION)

        return self._description

    def __str__ (self):
        return f'{str(self.label)} ({self.identifier})'



class EntityTranslation (models.Model):
    class Meta:
        indexes = [
            models.Index(fields=['text']),
            models.Index(fields=['selected', 'text']),
            models.Index(fields=['entity', 'field', 'selected'])
        ]

    class EntityTranslationField(models.IntegerChoices):
        LABEL = 0
        DESCRIPTION = 1

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_text = self.text

    created_on = models.DateTimeField(auto_now_add=True)
    key = models.TextField()
    entity = models.ForeignKey(Entity, on_delete=models.CASCADE, related_name='+')
    language = models.ForeignKey(Language, on_delete=models.CASCADE, related_name='+')
    field = models.IntegerField(choices=EntityTranslationField.choices)
    selected = models.BooleanField()
    text = models.TextField()

    """
        When a text changes a copy of the old translation is kept but
        no longer marked as selected.

        1 - Save model with old text, selected marked False
        2 - Insert a new record with the text, and marked as selected
    """
    def save(self, **kwargs):
        if self.pk and self.text != self.__original_text:
            # Disable old copy
            new_text = self.text
            self.text = self.__original_text
            self.selected = False
            super().save(*kwargs)
            
            # Force creation of a new record which is selected
            self.pk = None
            self.text = new_text
            self.selected = True
            
        return super().save(*kwargs)

    def __str__ (self):
        if self.field == self.EntityTranslationField.LABEL:
            return f'Label'
        elif self.field == self.EntityTranslationField.DESCRIPTION:
            return f'Description'


class TranslatedItemManager (TranslatedEntityManager):
    def get_queryset(self) -> QuerySet:
        return super().get_queryset().filter(type=Entity.EntityType.ITEM)


class Item (Entity):
    class Meta ():
        proxy = True

    objects = TranslatedItemManager()


class TranslatedPropertyManager (TranslatedEntityManager):
    def get_queryset(self) -> QuerySet:
        return super().get_queryset().filter(type=Entity.EntityType.PROPERTY)


class Property (Entity):
    class Meta ():
        proxy = True
        verbose_name_plural = "properties"

    objects = TranslatedPropertyManager()


class EntityClaim (models.Model):
    class Meta:
        verbose_name = 'Claim'

    type = models.CharField(max_length=50)
    text = models.TextField(blank=True, null=True)
    extra_data = models.JSONField(blank=True, null=True)

    subject = models.ForeignKey(Entity, related_name='claims', on_delete=models.CASCADE)
    property = models.ForeignKey(Property, on_delete=models.PROTECT, related_name='property_in_claims') # , limit_choices_to={'type': Entity.EntityType.PROPERTY}
    object = models.ForeignKey(Entity, blank=True, null=True, on_delete=models.PROTECT, related_name='object_in_claims')


class EntityClaimQualifier (models.Model):
    class Meta:
        verbose_name = 'Qualifier'

    type = models.CharField(max_length=50)
    text = models.TextField(blank=True, null=True)
    extra_data = models.JSONField(blank=True, null=True)
    order = models.PositiveSmallIntegerField()

    subject = models.ForeignKey(EntityClaim, related_name='qualifiers', on_delete=models.CASCADE)
    property = models.ForeignKey(Property, on_delete=models.PROTECT, related_name='+') # , limit_choices_to={'type': Entity.EntityType.PROPERTY}
    object = models.ForeignKey(Entity, blank=True, null=True, on_delete=models.PROTECT, related_name='+')


class EntityClaimReference (models.Model):
    entity_claim = models.ForeignKey(EntityClaim, related_name='references', on_delete=models.CASCADE)


class EntityClaimReferenceClaim (models.Model):
    type = models.CharField(max_length=50)
    text = models.TextField(blank=True, null=True)
    extra_data = models.JSONField(blank=True, null=True)

    subject = models.ForeignKey(EntityClaimReference, related_name='claims', on_delete=models.CASCADE)
    property = models.ForeignKey(Property, on_delete=models.PROTECT, related_name='+') # , limit_choices_to={'type': Entity.EntityType.PROPERTY}
    object = models.ForeignKey(Entity, blank=True, null=True, on_delete=models.PROTECT, related_name='+')


class TranslatedFlatPageManager (models.Manager):
    def get_queryset(self):
        # return super().get_queryset().select_related('label', 'description')

        # Use ugly field names to allow for less ugly property functions
        # which are required for the admin?
        return super().get_queryset().annotate(
            _slug = select_flat_page_translation_subquery('pk', FlatPageTranslation.FlatPageTranslationField.SLUG),
            _title = select_flat_page_translation_subquery('pk', FlatPageTranslation.FlatPageTranslationField.TITLE),
            _content = select_flat_page_translation_subquery('pk', FlatPageTranslation.FlatPageTranslationField.CONTENT)
        )


class FlatPage (models.Model):
    name = models.SlugField()
    published = models.BooleanField(default=False)

    @property
    def slug (self):
        if not hasattr(self, '_slug'):
            self._slug = select_flat_page_field_translation(self.pk, FlatPageTranslation.FlatPageTranslationField.SLUG)
        
        return self._slug

    @property
    def title (self):
        if not hasattr(self, '_title'):
            self._title = select_flat_page_field_translation(self.pk, FlatPageTranslation.FlatPageTranslationField.TITLE)

        if not hasattr(self, '_formatted_title'):
            self._formatted_title = mark_safe(
                Template(
                    re.sub(r'<p>(.+)</p>', '\\1', markdown.markdown(self._title, extensions=['extra']))
                ).render(Context({})))
        return self._formatted_title

    @property
    def content (self):
        if not hasattr(self, '_content'):
            self._content = select_flat_page_field_translation(self.pk, FlatPageTranslation.FlatPageTranslationField.CONTENT)

        if not hasattr(self, '_formatted_content'):
            self._formatted_content = mark_safe(Template(markdown.markdown(self._content, extensions=['extra'])).render(Context({})))

        return self._formatted_content


class FlatPageTranslation (models.Model):
    class Meta:
        indexes = [
            models.Index(fields=['text']),
            models.Index(fields=['selected', 'text']),
            models.Index(fields=['page', 'field', 'selected'])
        ]

    class FlatPageTranslationField(models.IntegerChoices):
        SLUG = 0
        TITLE = 1
        CONTENT = 2

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_text = self.text

    created_on = models.DateTimeField(auto_now_add=True)
    key = models.TextField()
    page = models.ForeignKey(FlatPage, on_delete=models.CASCADE, related_name='+')
    language = models.ForeignKey(Language, on_delete=models.CASCADE, related_name='+')
    field = models.IntegerField(choices=FlatPageTranslationField.choices)
    selected = models.BooleanField()
    text = models.TextField()

    """
        When a text changes a copy of the old translation is kept but
        no longer marked as selected.

        1 - Save model with old text, selected marked False
        2 - Insert a new record with the text, and marked as selected
    """
    def save(self, **kwargs):
        if self.pk and self.text != self.__original_text:
            # Disable old copy
            new_text = self.text
            self.text = self.__original_text
            self.selected = False
            super().save(*kwargs)
            
            # Force creation of a new record which is selected
            self.pk = None
            self.text = new_text

        self.selected = True
        return super().save(*kwargs)

    def __str__ (self):
        return self.text


# Change default field value. Is here just for admin.
# Explore wether it is easier to make default a callable
# and to set some kind of internal field on this model?
class FlatPageSlug (FlatPageTranslation):
    def __init__(self, *args, **kwargs):
        self._meta.get_field('field').default = FlatPageTranslation.FlatPageTranslationField.SLUG
        super().__init__(*args, **kwargs)

    class Meta ():
        proxy = True


# Change default field value. Is here just for admin
class FlatPageTitle (FlatPageTranslation):
    def __init__(self, *args, **kwargs):
        self._meta.get_field('field').default = FlatPageTranslation.FlatPageTranslationField.TITLE
        super().__init__(*args, **kwargs)

    class Meta ():
        proxy = True


# Change default field value. Is here just for admin
class FlatPageContent (FlatPageTranslation):
    def __init__(self, *args, **kwargs):
        self._meta.get_field('field').default = FlatPageTranslation.FlatPageTranslationField.CONTENT
        super().__init__(*args, **kwargs)

    class Meta ():
        proxy = True

class LanguageMapper (object):
    def __init__ (self):
        self.language_ids = None

    def get (self, code):
        if not self.language_ids:
            self.language_ids = { r['code']: r['pk'] for r in Language.objects.all().values('pk', 'code') }

        if code in self.language_ids:
            return self.language_ids[code]
        else:
            return self.language_ids[FALLBACK_LANGUAGE]

languageMap = LanguageMapper()


"""
    Subquery to select translation for a term_id in the field
    set through outername.

    The subquery selects rows for the given term and then orders
    them by language preference where the PEFERRED_LANGUAGE should
    show up before the FALLBACK_LANGUAGE. 
"""
def select_entity_translation_subquery(outername, field):
    return Subquery(
        EntityTranslation
            .objects
            .filter(entity_id=OuterRef(outername), field=field, selected=True)
            .order_by(Case(
                When(language_id=languageMap.get(get_language()), then=Value(1)),
                When(language_id=languageMap.get(settings.FALLBACK_LANGUAGE), then=Value(2)),
                default=Value(3)))
            .values('text')[:1] # Only have 1 row & 1 value
    )


def select_entity_field_translation(entity_pk, field):
    queryset = (
        EntityTranslation.objects
            .filter(entity_id=entity_pk, field=field, selected=True)
            .order_by(Case(
                When(language_id=languageMap.get(get_language()), then=Value(1)),
                When(language_id=languageMap.get(settings.FALLBACK_LANGUAGE), then=Value(2)),
                default=Value(3)))
            .values('text')[:1] # Only have 1 row & 1 value
        ).first()
    if queryset:
        return queryset['text']
    else:
        return None

def select_flat_page_translation_subquery(outername, field):
    return Subquery(
        FlatPageTranslation
            .objects
            .filter(page_id=OuterRef(outername), field=field, selected=True)
            .order_by(Case(
                When(language_id=languageMap.get(get_language()), then=Value(1)),
                When(language_id=languageMap.get(settings.FALLBACK_LANGUAGE), then=Value(2)),
                default=Value(3)))
            .values('text')[:1] # Only have 1 row & 1 value
    )


def select_flat_page_field_translation(page_pk, field):
    queryset = (
        FlatPageTranslation.objects
            .filter(page_id=page_pk, field=field, selected=True)
            .order_by(Case(
                When(language_id=languageMap.get(get_language()), then=Value(1)),
                When(language_id=languageMap.get(settings.FALLBACK_LANGUAGE), then=Value(2)),
                default=Value(3)))
            .values('text')[:1] # Only have 1 row & 1 value
        ).first()
    if queryset:
        return queryset['text']
    else:
        return None

def annotate_translation (qs):
    return qs.annotate(
        _label = select_entity_translation_subquery('pk', EntityTranslation.EntityTranslationField.LABEL),
        _description = select_entity_translation_subquery('pk',  EntityTranslation.EntityTranslationField.DESCRIPTION)
    )