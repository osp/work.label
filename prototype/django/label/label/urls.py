"""
URL configuration for label project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from . import views
from django.contrib import admin
from django.urls import path, include
from debug_toolbar.toolbar import debug_toolbar_urls

urlpatterns = [
    path('entity/<str:pk>/', views.entity, name='entity'),
    path('entities/', views.entities, name='entities'),
    path('entities/<int:pk>', views.values, name='values'),
    path('entities/<str:query>', views.entities, name='entities'),

    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
    path('index/<str:query>', views.index, name='index'),


    # path('page/about', views.about, name='about'),

    path('admin/', admin.site.urls),
    path("i18n/", include("django.conf.urls.i18n")),
    
    # Catch all to try and find a page
    path("<str:slug>", views.page, name='page')

] + debug_toolbar_urls()
