from django.conf import settings


def format_date (date, precision):
    return date.strftime(settings.LABEL_DATE_FORMATS[precision])